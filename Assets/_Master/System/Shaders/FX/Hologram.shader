Shader "TOZ/Object/FX/Hologram" {
	Properties {
		_MainTex("Base (RGB)", 2D) = "white" {}
		[NoScaleOffset] _HoloTex("Hologram Texture", 2D) = "white" { }
		_Power("Power", Range (0.0, 1)) = 0.5
		_Speed("Speed", Range (0.0, 100)) = 15
		_Thickness("Thickness", Range (0.0, 3)) = 2.0
		_Luminance("Luminance", Range (0, 1)) = 0.25
		_Darkness("Darkness", Range (0, 1)) = 0.75
	}

	SubShader {
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" "IgnoreProjector" = "True" }
		LOD 100

		Pass {
			Name "BASE"
			Tags { "LightMode" = "Always" }

			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMask RGB

			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma target 2.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#pragma multi_compile_instancing

			sampler2D _MainTex, _HoloTex;
			fixed _Power, _Speed, _Thickness, _Luminance, _Darkness;

			struct a2v {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 eye : TEXCOORD1;
				float3 norm : TEXCOORD2;
				UNITY_FOG_COORDS(3)
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			v2f vert(a2v v) {
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord.xy;
				o.eye = normalize(ObjSpaceViewDir(v.vertex));
				o.norm = normalize(v.normal);
				UNITY_TRANSFER_FOG(o, o.pos);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target {
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 main = tex2D(_MainTex, i.uv);

				//For simple meshes, this should be calculated in vert
				fixed EdotN = clamp(dot(i.eye, i.norm), 0.0, 1.0);
				fixed3 holo = tex2D(_HoloTex, EdotN.xx).rgb;
				fixed alpha = EdotN * main.a;
				fixed4 result = fixed4(lerp(main.rgb, holo, _Power), alpha);
				float cycle = sin((i.uv.y / 0.005 - _Time.y * _Speed) * _Thickness);
				result *= _Darkness + _Luminance * cycle;
				UNITY_APPLY_FOG(i.fogCoord, result);
				return result;
			}
			ENDCG
		}
	}

	Fallback Off
}