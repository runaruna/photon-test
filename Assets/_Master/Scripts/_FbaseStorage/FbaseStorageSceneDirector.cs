﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Storage;
using UnityEngine.XR.iOS;


public class FbaseStorageSceneDirector : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Text m_LogText;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	protected FirebaseStorage m_FirebaseStorage;
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		m_FirebaseStorage = Firebase.Storage.FirebaseStorage.DefaultInstance; //FirebaseStorageのインスタンスをセット
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		m_LogText.text += "-----------\n";
		m_LogText.text += " Start\n";
		m_LogText.text += "-----------\n";
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region アップロード
	/// <summary>
	/// データをFireBase Storageにアップデート
	/// </summary>
	public void uploadData()
	{
		Debug.Log("mob- upd-");
		m_LogText.text += "mob- upd-\n";

		string fname = "worldMap.worldmap";
		string fpath = string.Empty;

		StorageReference storageRef = m_FirebaseStorage.GetReferenceFromUrl("gs://photon-test-262ff.appspot.com");
		StorageReference arworldmapsRef = storageRef.Child("arworldmaps/worldMap.worldmap");

#if !UNITY_EDITOR && UNITY_IOS

		fpath = Application.persistentDataPath+"/"+fname;

		Debug.Log("mob- fpath : " + fpath);
		Debug.Log("mob- fpath exists : " + File.Exists(fpath));

		m_LogText.text += "mob- fpath : "+fpath+"\n";
		m_LogText.text += "mob- fpath exists : "+File.Exists(fpath)+"\n";

		ARWorldMap loadedWorldMap = ARWorldMap.Load(fpath);
		byte[] serializedData = loadedWorldMap.SerializeToByteArray();

		Task<StorageMetadata> task = arworldmapsRef.PutBytesAsync(serializedData, null, new StorageProgress<UploadState>(state =>
		{
			// called periodically during the upload
			Debug.Log(string.Format(
				"Progress: {0} of {1} bytes transferred.",
				state.BytesTransferred,
				state.TotalByteCount
			));
			m_LogText.text += string.Format("Progress: {0} of {1} bytes transferred.",state.BytesTransferred,state.TotalByteCount)+"\n";
		}), CancellationToken.None, null);
		task.ContinueWith(resTask =>
		{
			if (resTask.IsFaulted || resTask.IsCanceled)
			{
				Debug.Log(resTask.Exception.ToString());
				m_LogText.text += resTask.Exception.ToString() + "\n";
			}
			else
			{
				StorageMetadata metadata = task.Result;
				Debug.Log("Finished uploading...");
				m_LogText.text += "Finished uploading...\n";
			}
		});

#else

		fpath = Application.dataPath + "/___WordMapData/" + fname;

		Debug.Log("mob- fpath : " + fpath);
		Debug.Log("mob- fpath exists : " + File.Exists(fpath));

		m_LogText.text += "mob- fpath : " + fpath + "\n";
		m_LogText.text += "mob- fpath exists : " + File.Exists(fpath) + "\n";

		Task<StorageMetadata> task = arworldmapsRef.PutFileAsync(fpath, null, new StorageProgress<UploadState>(state => 
		{
			// called periodically during the upload
			Debug.Log(string.Format(
				"Progress: {0} of {1} bytes transferred.",
				state.BytesTransferred,
				state.TotalByteCount
			));
			m_LogText.text += string.Format("Progress: {0} of {1} bytes transferred.", state.BytesTransferred, state.TotalByteCount) + "\n";
		}), CancellationToken.None, null);

		task.ContinueWith(resTask =>
		{
			if (resTask.IsFaulted || resTask.IsCanceled)
			{
				Debug.Log(resTask.Exception.ToString());
				m_LogText.text += resTask.Exception.ToString() + "\n";
			}
			else
			{
				StorageMetadata metadata = task.Result;
				Debug.Log("Finished uploading...");
				m_LogText.text += "Finished uploading...\n";
			}
		});

		#endif
		Debug.Log("mob- upd- enp-");
		m_LogText.text += "mob- upd- enp-\n";
	}
	/// <summary>
	/// FireBaseにアップロードされているファイルをbyte型の配列にダウンロード
	/// </summary>
	public void downloadData()
	{
		Debug.Log("mob- dl-");
		m_LogText.text += "mob- dl-\n";

		StorageReference dlRef = m_FirebaseStorage.GetReferenceFromUrl("gs://photon-test-262ff.appspot.com/arworldmaps/worldMap.worldmap");

		long maxAllowedSize = 10 * 1024 * 1024; //10MBまでのデータだけダウンロードできるように制約をかける
		Task<byte[]> task = dlRef.GetBytesAsync(maxAllowedSize, new StorageProgress<DownloadState>((DownloadState state) =>
		{
			Debug.Log(string.Format(
				"Progress: {0} of {1} bytes transferred.",
				state.BytesTransferred,
				state.TotalByteCount
			));

			m_LogText.text += string.Format("Progress: {0} of {1} bytes transferred.",state.BytesTransferred,state.TotalByteCount)+"\n";

		}), CancellationToken.None);
		task.ContinueWith(resTask =>
		{
			if(resTask.IsFaulted || resTask.IsCanceled)
			{
				Debug.Log(resTask.Exception.ToString());
				m_LogText.text += resTask.Exception.ToString() + "\n";
			}
			else
			{
				byte[] fileContents = task.Result;
				Debug.Log("Finished downloading!");
				Debug.Log("fileContents.length : " + fileContents.Length);

				m_LogText.text += "Finished downloading!\n";
				m_LogText.text += "fileContents.length : "+fileContents.Length+"\n";

			}
		});

		Debug.Log("mob- dl- enp-");
		m_LogText.text += "mob- dl- enp-\n";
	}
	#endregion //アップロード

	// --------
	#region ダウンロード
	#endregion //ダウンロード

	// --------
	#region メンバメソッド
	/// <summary>
	/// ログテキストの内容を消去
	/// </summary>
	public void deleteLog()
	{
		m_LogText.text = string.Empty;
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
