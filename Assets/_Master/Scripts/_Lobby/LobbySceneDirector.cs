﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class LobbySceneDirector : BasePunSceneDirector
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region 定数
	/// <summary>
	/// 
	/// </summary>
	protected static string ROOMNAME_PREFIX = "IENEKO";
	/// <summary>
	/// 
	/// </summary>
	protected static string ROOMNAME_DELIMITER = "-";
	#endregion //定数

	// --------
	#region インスペクタ設定用フィールド
	[Header("*PlayerCard Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PlyaerCardHolder;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform PlayerCardHolderTf
	{
		get  { return m_PlyaerCardHolder.transform; }
	}
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PlayerCardPrefab;
	[Header("*UI LoginUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitLoginUIGroup;
	[Header("*UI NickNameInputFiled")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected InputField m_PortraitNickNameInputField;
	[Header("*UI HomeUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitHomeUIGroup;
	[Header("*UI InsideRoomUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitInsideUIGroup;
	[Header("*UI BtnStartGame")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitBtnStartGame;
	[Header("*UI RoomNameText")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Text m_PortraitRoomNameText;
	[Header("*UI JoinRoomUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitJoinRoomUIGroup;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected InputField m_PortraitRoomNameInputField;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 入室する部屋名を格納するフィールド
	/// </summary>
	protected string m_RoomName;
	/// <summary>
	/// 
	/// </summary>
	protected Dictionary<int, GameObject> m_PlayerCardDict;
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected override void Awake()
	{
		base.Awake();

		m_PlayerCardDict = new Dictionary<int, GameObject>();

		showLoginUIGroup();
		m_PortraitBtnStartGame.SetActive(false);
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected override void Start ()
	{
		base.Start();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void Update ()
	{
		base.Update();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void LateUpdate()
	{
		base.LateUpdate();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region PUN コールバック
	/// <summary>
	/// PhotonNetwork.autoJoinLobby が false の状態で、
	/// マスターサーバーへ接続および認証が完了した際に呼び出されます。
	/// </summary>
	public override void OnConnectedToMaster()
	{
		m_PortraitLoginUIGroup.SetActive(false);
		m_PortraitHomeUIGroup.SetActive(true);

		// PhotonNetwork.JoinLobby();

		base.OnConnectedToMaster();
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="cause"></param>
	public override void OnDisconnected(DisconnectCause cause)
	{
		showLoginUIGroup();
		m_PortraitBtnStartGame.SetActive(false);

		base.OnDisconnected(cause);
	}
	/// <summary>
	/// Master Serverのロビーに入るときに呼び出されます
	/// </summary>
	public override void OnJoinedLobby()
	{
		base.OnJoinedLobby();
	}
	/// <summary>
	/// ロビーを退出した際に呼び出されます。
	/// </summary>
	public override void OnLeftLobby()
	{
		base.OnLeftLobby();
	}
	/// <summary>
	/// Master Serverのロビー（InLobby）にいる間に、ルームリストが更新されるたびに呼び出されます。
	/// </summary>
	/// <param name="roomList"></param>
	public override void OnRoomListUpdate(List<RoomInfo> roomList)
	{
		base.OnRoomListUpdate(roomList);
	}
	/// <summary>
	/// ルームの作成に成功したときに呼び出される
	/// </summary>
	public override void OnCreatedRoom()
	{
		m_PortraitRoomNameText.text = m_RoomName;
		base.OnCreatedRoom();
	}
	/// <summary>
	/// ルームの作成に失敗したときに呼び出される
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnCreateRoomFailed(short returnCode, string message)
	{
		base.OnCreateRoomFailed(returnCode, message);
	}
	/// <summary>
	/// 部屋に入室した際に呼ばれる
	/// </summary>
	public override void OnJoinedRoom()
	{
		m_PortraitRoomNameText.text = m_RoomName;

		foreach (Player p in PhotonNetwork.PlayerList)
		{
			GameObject pCard = (GameObject)Instantiate(m_PlayerCardPrefab);
			pCard.transform.parent = PlayerCardHolderTf;
			pCard.transform.localScale = Vector3.one;
			pCard.GetComponent<PlayerCardController>().m_PlayerNameTextStr = p.NickName;
			m_PlayerCardDict.Add(p.ActorNumber, pCard);
		}

		if(PhotonNetwork.IsMasterClient)
		{
			m_PortraitBtnStartGame.SetActive(true);
		}
		else
		{
			m_PortraitBtnStartGame.SetActive(false);
		}

		showInsideRoomUIGroup();

		base.OnJoinedRoom();
	}
	/// <summary>
	/// ルームの入室に失敗したときに呼び出される
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRoomFailed(short returnCode, string message)
	{
		base.OnJoinRandomFailed(returnCode, message);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		base.OnJoinRandomFailed(returnCode, message);
	}
	/// <summary>
	/// 部屋から退出した際に呼ばれる
	/// </summary>
	public override void OnLeftRoom()
	{
		showHomeUIGroup();

		foreach (KeyValuePair<int,GameObject> kvp in m_PlayerCardDict)
		{
			Destroy(kvp.Value);
		}
		m_PlayerCardDict.Clear();

		base.OnLeftRoom();
	}
	/// <summary>
	/// リモートプレイヤーが部屋に入室した際に呼ばれる
	/// (このプレーヤーは既にプレーヤーリストに追加されています)
	/// </summary>
	/// <param name="newPlayer"></param>
	public override void OnPlayerEnteredRoom(Player newPlayer)
	{
		GameObject pCard = (GameObject)Instantiate(m_PlayerCardPrefab);
		pCard.transform.parent = PlayerCardHolderTf;
		pCard.transform.localScale = Vector3.one;
		pCard.GetComponent<PlayerCardController>().m_PlayerNameTextStr = newPlayer.NickName;
		m_PlayerCardDict.Add(newPlayer.ActorNumber, pCard);

		base.OnPlayerEnteredRoom(newPlayer);
	}
	/// <summary>
	/// リモートプレイヤーが部屋を離れるか、非アクティブになったときに呼び出されます。
	/// </summary>
	/// <param name="otherPlayer"></param>
	public override void OnPlayerLeftRoom(Player otherPlayer)
	{

		Destroy(m_PlayerCardDict[otherPlayer.ActorNumber]);
		m_PlayerCardDict.Remove(otherPlayer.ActorNumber);

		base.OnPlayerLeftRoom(otherPlayer);
	}
	/// <summary>
	/// 現在のMasterClientが終了したときに新しいMasterClientに切り替えた後に呼び出されます。
	/// </summary>
	/// <param name="newMasterClient"></param>
	public override void OnMasterClientSwitched(Player newMasterClient)
	{

		if (PhotonNetwork.IsMasterClient)
		{
			m_PortraitBtnStartGame.SetActive(true);
		}
		else
		{
			m_PortraitBtnStartGame.SetActive(false);
		}

		base.OnMasterClientSwitched(newMasterClient);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="target"></param>
	/// <param name="changedProps"></param>
	public override void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
	{
		base.OnPlayerPropertiesUpdate(target, changedProps);
	}
	#endregion //PUN コールバック

	// --------
	#region UIGroup
	/// <summary>
	/// 
	/// </summary>
	public void showLoginUIGroup()
	{
		m_PortraitLoginUIGroup.SetActive(true);
		m_PortraitHomeUIGroup.SetActive(false);
		m_PortraitInsideUIGroup.SetActive(false);
		m_PortraitJoinRoomUIGroup.SetActive(false);
	}
	/// <summary>
	/// 
	/// </summary>
	public void showHomeUIGroup()
	{
		m_PortraitLoginUIGroup.SetActive(false);
		m_PortraitHomeUIGroup.SetActive(true);
		m_PortraitInsideUIGroup.SetActive(false);
		m_PortraitJoinRoomUIGroup.SetActive(false);
	}
	/// <summary>
	/// 
	/// </summary>
	public void showInsideRoomUIGroup()
	{
		m_PortraitLoginUIGroup.SetActive(false);
		m_PortraitHomeUIGroup.SetActive(false);
		m_PortraitInsideUIGroup.SetActive(true);
		m_PortraitJoinRoomUIGroup.SetActive(false);
	}
	/// <summary>
	/// 
	/// </summary>
	public void showJoinRoomUIGroup()
	{
		m_PortraitLoginUIGroup.SetActive(false);
		m_PortraitHomeUIGroup.SetActive(false);
		m_PortraitInsideUIGroup.SetActive(false);
		m_PortraitJoinRoomUIGroup.SetActive(true);
	}
	#endregion //UIGroup

	// --------
	#region ログイン
	/// <summary>
	/// Photonのマスターサーバに接続
	/// </summary>
	public void connect()
	{
		PhotonNetwork.LocalPlayer.NickName = m_PortraitNickNameInputField.text;
		PhotonNetwork.ConnectUsingSettings();
	}
	#endregion //ログイン

	// --------
	#region ログアウト
	/// <summary>
	/// Photonのマスターサーバへの接続を解除
	/// </summary>
	public void disconnect()
	{
		if(PhotonNetwork.IsConnected)
		{
			PhotonNetwork.Disconnect();
		}
	}
	#endregion //ログアウト

	// --------
	#region 部屋作成
	public void createRoom()
	{
		// m_RoomName = ROOMNAME_PREFIX + ROOMNAME_DELIMITER + createRadomCode(8) + ROOMNAME_DELIMITER + getTimestamp();
		m_RoomName = ROOMNAME_PREFIX + ROOMNAME_DELIMITER + createRadomCode(8);

		PhotonNetwork.CreateRoom(m_RoomName);
	}
	#endregion //部屋作成

	// --------
	#region 部屋への入退室
	/// <summary>
	/// 部屋に入室する
	/// </summary>
	public void joinRoom()
	{
		m_RoomName = m_PortraitRoomNameInputField.text;
		PhotonNetwork.JoinRoom(m_RoomName);
	}
	/// <summary>
	/// 部屋から退出してマスターサーバへ戻る
	/// </summary>
	public void leaveRoom()
	{
		PhotonNetwork.LeaveRoom();
	}
	#endregion //部屋への入退室

	// --------
	#region ゲーム開始
	/// <summary>
	/// 
	/// </summary>
	public void startGame()
	{
		PhotonNetwork.CurrentRoom.IsOpen = false;
		PhotonNetwork.CurrentRoom.IsVisible = false;

		PhotonNetwork.LoadLevel("MultiAR");
	}
	#endregion //ゲーム開始

	// --------
	#region メンバメソッド
	/// <summary>
	/// タイムスタンプを取得する関数
	/// </summary>
	protected string getTimestamp()
	{
		string res = string.Empty;
		uint timestamp = (uint)(DateTime.UtcNow - new DateTime(1970,1,1,0,0,0, DateTimeKind.Utc)).TotalSeconds;
		res = timestamp.ToString();
		return res;
	}
	/// <summary>
	/// ランダムな文字列のコードを生成する関数
	/// </summary>
	/// <param name="_len">生成するコードの文字数</param>
	/// <returns></returns>
	protected string createRadomCode(int _len)
	{
		string res = string.Empty;
		string baseChars = "0123456789ABCDF";//生成するのに使う文字
		StringBuilder sb = new StringBuilder();
		System.Random r = new System.Random();

		for(int i=0; i < _len; i++)
		{
			//文字の位置をランダムに選択
			int pos = r.Next(baseChars.Length);
			//選択された位置の文字を取得
			char c = baseChars[pos];
			//パスワードに追加
			sb.Append(c);
		}
		res = sb.ToString();
		return res;
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
