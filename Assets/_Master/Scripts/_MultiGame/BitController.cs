﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BitController : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	[Header("*Movement Setting")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_DistinationObj;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected PhotonView m_PhotonView;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected float m_MoveSpeed = 3.0f;
	/// <summary> 
	/// 
	/// </summary>
	[SerializeField] protected float m_RotateSpeed = 60.0f;
	[Header("*Skin Setting")]
	/// <summary> 
	/// 
	/// </summary>
	[SerializeField] protected Renderer m_Renderer;
	/// <summary> 
	/// 
	/// </summary>
	[SerializeField] protected Material[] m_MaterialList;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary> 
	/// 
	/// </summary>
	protected Transform m_PlayerTransform;
	/// <summary>
	/// 
	/// </summary>
	protected Vector3 m_PlayerMoveVelocity;
	/// <summary> 
	/// 
	/// </summary>
	protected Vector3 m_PlayerMoveDirection;
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		m_PlayerTransform = this.gameObject.transform;
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{
		if(!m_PhotonView.IsMine)
		{
			return;
		}

		if(Input.GetKeyDown(KeyCode.R))
		{
			chgSkin(0);
		}
		else if(Input.GetKeyDown(KeyCode.G))
		{
			chgSkin(1);
		}
		else if(Input.GetKeyDown(KeyCode.B))
		{
			chgSkin(2);
		}

		playerMoveUpdate();
		playerRotateUpdate();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{
		lifeCycleUpdate();
	}
	/// <summary>
	/// the on trigger enter.
	/// </summary>
	protected virtual void OnTriggerEnter()
	{

	}
	/// <summary>
	/// the on trigger stay.
	/// </summary>
	protected virtual void OnTriggerStay()
	{

	}
	/// <summary>
	/// the on trigger exit.
	/// </summary>
	protected virtual void OnTriggerExit()
	{

	}
	/// <summary>
	/// the on collision enter.
	/// </summary>
	protected virtual void OnCollisionEnter(Collision other)
	{

	}
	/// <summary>
	/// the on collision stay.
	/// </summary>
	protected virtual void OnCollisionStay(Collision other)
	{

	}
	/// <summary>
	/// the on collision exit.
	/// </summary>
	protected virtual void OnCollisionExit(Collision other)
	{

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region スキン関係
	/// <summary>
	/// 
	/// </summary>
	/// <param name="_skinNum"></param>
	protected void chgSkin(int _skinNum)
	{
		// m_Renderer.material = m_MaterialList[_skinNum];
		m_PhotonView.RPC("RpcChgSkin", RpcTarget.All, _skinNum);
	}
	/// <summary>
	/// [RPC]
	/// </summary>
	[PunRPC]
	protected void RpcChgSkin(int _skinNum)
	{
		Debug.Log("mob- rpcchgskin- _skinNum : "+_skinNum);
		m_Renderer.material = m_MaterialList[_skinNum];
	}
	#endregion //スキン関係

	// --------
	#region 移動関係
	
	#endregion //移動関係

	// --------
	#region LifeCycle
	/// <summary>
	/// 
	/// </summary>
	protected void lifeCycleUpdate()
	{
		
	}
	#endregion //LifeCycle

	// --------
	#region メンバメソッド
	/// <summary>
	/// プレイヤー回転処理
	/// </summary>
	protected void playerRotateUpdate()
	{
		Vector3 m_NewAngle = new Vector3(
			m_PlayerTransform.localEulerAngles.x,
			m_PlayerTransform.localEulerAngles.y,
			m_PlayerTransform.localEulerAngles.z
		);

		//左回転
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			m_NewAngle.y -= m_RotateSpeed * Time.deltaTime;
		}
		//右回転
		if (Input.GetKey(KeyCode.RightArrow))
		{
			m_NewAngle.y += m_RotateSpeed * Time.deltaTime;
		}

		m_PlayerTransform.localEulerAngles = m_NewAngle;

	}

	/// <summary>
	/// プレイヤー移動処理
	/// </summary>
	protected virtual void playerMoveUpdate()
	{

		m_PlayerMoveVelocity = Vector3.zero;
		m_PlayerMoveDirection = Vector3.zero;

		if (Input.GetKey(KeyCode.W) &&
		 (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
		{
			m_PlayerMoveDirection.y += 1.0f; //上平行移動
		}
		else if (Input.GetKey(KeyCode.W))
		{
			m_PlayerMoveDirection.z += 1.0f; //前進
		}

		if (Input.GetKey(KeyCode.S) &&
		 (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
		{
			m_PlayerMoveDirection.y -= 1.0f; //上平行移動
		}
		else if (Input.GetKey(KeyCode.S))
		{
			m_PlayerMoveDirection.z -= 1.0f; //後退
		}

		if (Input.GetKey(KeyCode.A))
		{
			m_PlayerMoveDirection.x -= 1.0f; //左平行移動
		}

		if (Input.GetKey(KeyCode.D))
		{
			m_PlayerMoveDirection.x += 1.0f; //右平行移動
		}

		m_PlayerMoveVelocity = this.transform.TransformDirection(m_PlayerMoveDirection);
		m_PlayerMoveVelocity *= m_MoveSpeed; //移動スピード

		m_PlayerTransform.Translate(m_PlayerMoveVelocity, Space.World);
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
