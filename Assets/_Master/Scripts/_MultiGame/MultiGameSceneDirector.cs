﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;

public class MultiGameSceneDirector : BasePunSceneDirector
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	[Header("*RaiseEventManager Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected RaiseEventManager m_RaiseEventManager;
	[Header("*Stage Settings")]
	/// <summary>
	/// Stage prefab.
	/// </summary>
	[SerializeField] protected string m_StagePrefabName;
	[Header("*Bit Settings")]
	/// <summary>
	/// Bit prefab.
	/// </summary>
	[SerializeField] protected string m_BitPrefabName;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected override void Awake()
	{
		base.Awake();
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected override void Start()
	{
		base.Start();
		Debug.Log("mob- mgsd- CurrentRoomName : "+PhotonNetwork.CurrentRoom.Name);
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void Update()
	{
		if (Input.GetKeyDown(KeyCode.M))
		{
			m_RaiseEventManager.sendRaseEventMessageTest();
		}

		base.Update();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void LateUpdate()
	{
		base.LateUpdate();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region PUN コールバック
	/// <summary>
	/// PhotonNetwork.autoJoinLobby が false の状態で、
	/// マスターサーバーへ接続および認証が完了した際に呼び出されます。
	/// </summary>
	public override void OnConnectedToMaster()
	{
		base.OnConnectedToMaster();
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="cause"></param>
	public override void OnDisconnected(DisconnectCause cause)
	{
		base.OnDisconnected(cause);
		translateScenePortrait("Lobby");
	}
	/// <summary>
	/// Master Serverのロビーに入るときに呼び出されます
	/// </summary>
	public override void OnJoinedLobby()
	{
		base.OnJoinedLobby();
	}
	/// <summary>
	/// ロビーを退出した際に呼び出されます。
	/// </summary>
	public override void OnLeftLobby()
	{
		base.OnLeftLobby();
	}
	/// <summary>
	/// Master Serverのロビー（InLobby）にいる間に、ルームリストが更新されるたびに呼び出されます。
	/// </summary>
	/// <param name="roomList"></param>
	public override void OnRoomListUpdate(List<RoomInfo> roomList)
	{
		base.OnRoomListUpdate(roomList);
	}
	/// <summary>
	/// ルームの作成に成功したときに呼び出される
	/// </summary>
	public override void OnCreatedRoom()
	{
		base.OnCreatedRoom();
	}
	/// <summary>
	/// ルームの作成に失敗したときに呼び出される
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnCreateRoomFailed(short returnCode, string message)
	{
		base.OnCreateRoomFailed(returnCode, message);
	}
	/// <summary>
	/// ルームの入室に失敗したときに呼び出される
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRoomFailed(short returnCode, string message)
	{
		base.OnJoinRandomFailed(returnCode, message);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		base.OnJoinRandomFailed(returnCode, message);
	}
	/// <summary>
	/// 部屋に入室した際に呼ばれる
	/// </summary>
	public override void OnJoinedRoom()
	{
		base.OnJoinedRoom();
	}
	/// <summary>
	/// 部屋から退出した際に呼ばれる
	/// </summary>
	public override void OnLeftRoom()
	{
		base.OnLeftRoom();
		PhotonNetwork.Disconnect();
	}
	/// <summary>
	/// リモートプレイヤーが部屋に入室した際に呼ばれる
	/// (このプレーヤーは既にプレーヤーリストに追加されています)
	/// </summary>
	/// <param name="newPlayer"></param>
	public override void OnPlayerEnteredRoom(Player newPlayer)
	{
		base.OnPlayerEnteredRoom(newPlayer);
	}
	/// <summary>
	/// リモートプレイヤーが部屋を離れるか、非アクティブになったときに呼び出されます。
	/// </summary>
	/// <param name="otherPlayer"></param>
	public override void OnPlayerLeftRoom(Player otherPlayer)
	{
		base.OnPlayerLeftRoom(otherPlayer);
	}
	/// <summary>
	/// 現在のMasterClientが終了したときに新しいMasterClientに切り替えた後に呼び出されます。
	/// </summary>
	/// <param name="newMasterClient"></param>
	public override void OnMasterClientSwitched(Player newMasterClient)
	{
		base.OnMasterClientSwitched(newMasterClient);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="target"></param>
	/// <param name="changedProps"></param>
	public override void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
	{
		base.OnPlayerPropertiesUpdate(target,changedProps);
	}
	#endregion //PUN コールバック

	// --------
	#region ログアウト
	/// <summary>
	/// Photonのマスターサーバへの接続を解除
	/// </summary>
	public void disconnect()
	{
		if (PhotonNetwork.IsConnected)
		{
			PhotonNetwork.Disconnect();
		}
	}
	#endregion //ログアウト

	// --------
	#region ワールドマップ関係
	/// <summary>
	/// ボタン用ワールドマップ送信関数
	/// </summary>
	public void onClickedSendWorldMap()
	{
		if(PhotonNetwork.IsMasterClient)
		{
			m_RaiseEventManager.sendWorldMapTest();
		}
	}
	#endregion //ステージ関係

	// --------
	#region ステージ関係
	/// <summary>
	/// ステージ生成
	/// </summary>
	public void createStage()
	{
		if(PhotonNetwork.IsMasterClient)
		{
			PhotonNetwork.InstantiateSceneObject(m_StagePrefabName, Vector3.zero, Quaternion.identity, 0);
		}
	}
	#endregion //ステージ関係

	// --------
	#region Bit関係
	/// <summary>
	/// Bit生成
	/// </summary>
	public void createBit()
	{
		PhotonNetwork.Instantiate(m_BitPrefabName,Vector3.up,Quaternion.identity, 0);
	}
	#endregion //Bit関係

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
