﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.XR.iOS;

/// <summary>
/// 
/// </summary>
public class RaiseEventManager : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	/// <summary>
	/// 
	/// </summary>
	public void OnEnable()
	{
		Debug.Log("mob- OnEnable");
		PhotonNetwork.NetworkingClient.EventReceived += OnEvent; //RaiseEventの登録
	}
	/// <summary>
	/// 
	/// </summary>
	public void OnDisable()
	{
		Debug.Log("mob- OnDisable");
		PhotonNetwork.NetworkingClient.EventReceived -= OnEvent; //RaiseEventの破棄
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region RaiseEvent
	/// <summary>
	/// RaiseEvent
	/// </summary>
	protected void OnEvent(EventData _photonEvent)
	{
		Debug.Log("mob- mgs- oe- _photonEvent.Code : [" + _photonEvent.Code + "] _photonEvent.Sender : [" + _photonEvent.Sender + "]");

		switch (_photonEvent.Code)
		{
			case RaiseEventCodes.SendTest:
				Debug.Log("mob- mgs- oe- SendTest : " + (string)_photonEvent.CustomData);
			break;
			case RaiseEventCodes.SendWorldMap:
				Debug.Log("mob- mgs- oe- Receive World Map...");
				byte[] serializedWorldMap = (byte[])_photonEvent.CustomData;
				Debug.Log("mob- mgs- oe- serializedWorldMap.Length : "+serializedWorldMap.Length);
			break;
			default:
			break;
		}
	}
	/// <summary>
	/// 
	/// </summary>
	public void sendRaseEventMessageTest()
	{
		string message = "send RaseEvent...";
		var raiseEventOptions = new RaiseEventOptions()
		{
			Receivers = ReceiverGroup.All,
			CachingOption = EventCaching.DoNotCache
		};
		PhotonNetwork.RaiseEvent(RaiseEventCodes.SendTest, message, raiseEventOptions, SendOptions.SendReliable);
	}
	/// <summary>
	/// ワールドマップ送信テスト用関数
	/// </summary>
	public void sendWorldMapTest()
	{
		string fname = "worldMap.worldmap";
		string fpath = string.Empty;
		ARWorldMap loadedWorldMap;
		byte[] serializedWorldMap;

		#if !UNITY_EDITOR && UNITY_IOS
		fpath = Application.persistentDataPath+"/"+fname;
		#else
		fpath = Application.dataPath + "/___WordMapData/" + fname;
		#endif

Debug.Log("mob- swmt- fpath : "+fpath);

		loadedWorldMap = ARWorldMap.Load(fpath);
		serializedWorldMap = loadedWorldMap.SerializeToByteArray();

Debug.Log("mob- swmt- serializedWorldMap.Length : " + serializedWorldMap.Length);

		var raiseEventOptions = new RaiseEventOptions()
		{
			Receivers = ReceiverGroup.Others,
			CachingOption = EventCaching.DoNotCache
		};
		PhotonNetwork.RaiseEvent(RaiseEventCodes.SendWorldMap, serializedWorldMap, raiseEventOptions, SendOptions.SendReliable);
	}
	#endregion //RaiseEvent

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	/// <summary>
	/// RaiseEventCodes
	/// </summary>
	public class RaiseEventCodes
	{
		/// <summary>
		/// RaiseEvent送信テスト用コード
		/// </summary>
		public const byte SendTest = 0;
		/// <summary>
		/// WorldMap送信用コード
		/// </summary>
		public const byte SendWorldMap = 1;
	}
	#endregion //インナークラス
}
