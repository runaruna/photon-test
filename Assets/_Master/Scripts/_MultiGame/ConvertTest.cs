﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConvertTest : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		byte[] byte_before = new byte[3] { 0,1,2 };
		object obj;
		byte[] byte_after;

		Debug.Log("mob- byte_before.lenght : " + byte_before.Length);

		obj = byte_before;

		byte_after = (byte[])obj;

		Debug.Log("mob- byte_after.lenght : "+byte_after.Length);
		foreach (byte v in byte_after)
		{
			Debug.Log("mob- ct- v : "+v);
		}
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	/// <summary>
	/// the on trigger enter.
	/// </summary>
	protected virtual void OnTriggerEnter()
	{

	}
	/// <summary>
	/// the on trigger stay.
	/// </summary>
	protected virtual void OnTriggerStay()
	{

	}
	/// <summary>
	/// the on trigger exit.
	/// </summary>
	protected virtual void OnTriggerExit()
	{

	}
	/// <summary>
	/// the on collision enter.
	/// </summary>
	protected virtual void OnCollisionEnter(Collision other)
	{

	}
	/// <summary>
	/// the on collision stay.
	/// </summary>
	protected virtual void OnCollisionStay(Collision other)
	{

	}
	/// <summary>
	/// the on collision exit.
	/// </summary>
	protected virtual void OnCollisionExit(Collision other)
	{

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
