﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiStageMarkerController : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private Camera m_Camera;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private GameObject m_HoloStageHolderObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform HoloStageHolderTf 
	{
		get { return m_HoloStageHolderObj.transform; }
	}
	/// <summary> 
	/// 
	/// </summary>
	[SerializeField] private float followSpeed = 2.0f;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		//対象のカメラが指定されてない場合はMainCameraを対象にする。
		if (this.m_Camera == null) m_Camera = Camera.main;
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{
		RotationUpdate();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	/// <param name="_flg"></param>
	public void showHoloStageHolder(bool _flg)
	{
		m_HoloStageHolderObj.SetActive(_flg);
	}
	/// <summary>
	/// 回転更新処理
	/// </summary>
	void RotationUpdate()
	{

		//回転させる軸を制限
		Vector3 targetPos = m_Camera.transform.position;
		targetPos.y = HoloStageHolderTf.position.y;

		HoloStageHolderTf.LookAt(targetPos);

	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
