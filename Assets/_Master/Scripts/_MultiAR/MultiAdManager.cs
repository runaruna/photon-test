﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// マルチプレイ用オートドローン・マネージャ
/// </summary>
public class MultiAdManager : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	[Header("*AdRay Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private Camera m_Camera;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private GameObject m_AdRayOriginPrefab;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private float m_AdRayDist = 50.0f;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private string[] m_AdRayLayerList;
	/// <summary>
	/// 
	/// </summary>
	private int m_AdRayLayerMask;
	[Header("AdPlaceMarker Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private GameObject m_AdPlaceMarkerPrefab;
	[Header("MultiAd Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] string m_MultiAdPrefabName;
	[Header("*UI BtnPlaceAdActive")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] GameObject m_MasterClientBtnPlaceAdActive;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] GameObject m_ClientBtnPlaceAdActive;
	[Header("*UI BtnPlaceAdInActive")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] GameObject m_MasterClientBtnPlaceAdInActive;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] GameObject m_ClientBtnPlaceAdInActive;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region コモンメンバフィールド
	/// <summary>
	/// カレントモード
	/// 0:PLACE,1:DELETE,WAIT:-9999
	/// </summary>
	protected int m_CurrentMode = -9999;
	/// <summary>
	/// 初期化完了フラグ
	/// </summary>
	private bool m_InitCompFlag = false;
	/// <summary>
	/// [プロパティ] 初期化完了フラグ
	/// </summary>
	public bool InitCompFlag
	{
		get { return m_InitCompFlag; }
		private set { m_InitCompFlag = value; }
	}
	/// <summary>
	/// 処理中フラグ
	/// </summary>
	private bool m_ProcessingFlag = false;
	/// <summary>
	/// [プロパティ] 処理中フラグ
	/// </summary>
	public bool ProcessingFlag
	{
		get { return m_ProcessingFlag; }
		private set { m_ProcessingFlag = value; }
	}
	#endregion //コモンメンバフィールド

	// --------
	#region ステージ関係のメンバフィールド
	/// <summary>
	/// 
	/// </summary>
	private Transform m_StageTf;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform StageTf
	{
		get { return m_StageTf; }
		set { m_StageTf = value; }
	}
	#endregion //ステージ関係のメンバフィールド

	// --------
	#region Ad関係のメンバフィールド
	/// <summary>
	/// 
	/// </summary>
	private Dictionary<int, GameObject> m_AdObjDict;
	#endregion //Ad関係のメンバフィールド

	// --------
	#region AdRayOrigin関係のメンバフィールド
	/// <summary>
	/// 
	/// </summary>
	private GameObject m_AdRayOriginObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform AdRayOriginTf
	{
		get { return m_AdRayOriginObj.transform; }
	}
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public AdRayOriginController AdRayOriginCtrl
	{
		get { return m_AdRayOriginObj.GetComponent<AdRayOriginController>(); }
	}
	#endregion //AdRayOrigin関係のメンバフィールド

	// --------
	#region AdPlaceMarker関係のメンバフィールド
	/// <summary>
	/// 
	/// </summary>
	private GameObject m_AdPlaceMarkerObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform AdPlaceMarkerTf
	{
		get { return m_AdPlaceMarkerObj.transform; }
	}
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public AdPlaceMarkerController AdPlaceMarkerCtrl
	{
		get { return m_AdPlaceMarkerObj.GetComponent<AdPlaceMarkerController>(); }
	}
	#endregion //AdPlaceMarker関係のメンバフィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region UnityEvents
	[System.SerializableAttribute] public class CustomEventInt : UnityEvent<int> { }
	[Header("*モード変更時のイベント")]
	[SerializeField] public CustomEventInt m_OnModeChangeEvent;
	[Header("*自分自身のAutoDroneが設置された時のイベント")]
	[SerializeField] public CustomEventInt m_OnPlacedOwnAutoDroneEvent;
	#endregion //UnityEvents

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		m_AdObjDict = new Dictionary<int, GameObject>();
		m_AdRayLayerMask = LayerMask.GetMask(m_AdRayLayerList);
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{
		if(!InitCompFlag) return;

		switch (this.m_CurrentMode)
		{
			case AdManagerModeState.PLACE:
				this.AdRayOriginCtrl.showReticle(true);
				this.adPlaceRaycastUpdate();
			break;
			case AdManagerModeState.DELETE:
				this.AdRayOriginCtrl.showReticle(true);
			break;
			case AdManagerModeState.WAIT:
			default:
				this.AdRayOriginCtrl.showReticle(false);
				this.AdPlaceMarkerCtrl.showMarkerModel(false);
			break;
		}
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region ボタン関係の関数群
	/// <summary>
	/// 
	/// </summary>
	private void chgBtnPlaceAdActive(bool _flg)
	{
		if(_flg)
		{
			if(!m_MasterClientBtnPlaceAdActive.activeSelf) m_MasterClientBtnPlaceAdActive.SetActive(true);
			if(!m_ClientBtnPlaceAdActive.activeSelf) m_ClientBtnPlaceAdActive.SetActive(true);
			if(m_MasterClientBtnPlaceAdInActive.activeSelf) m_MasterClientBtnPlaceAdInActive.SetActive(false);
			if(m_ClientBtnPlaceAdInActive.activeSelf) m_ClientBtnPlaceAdInActive.SetActive(false);
		}
		else
		{
			if (m_MasterClientBtnPlaceAdActive.activeSelf) m_MasterClientBtnPlaceAdActive.SetActive(false);
			if (m_ClientBtnPlaceAdActive.activeSelf) m_ClientBtnPlaceAdActive.SetActive(false);
			if (!m_MasterClientBtnPlaceAdInActive.activeSelf) m_MasterClientBtnPlaceAdInActive.SetActive(true);
			if (!m_ClientBtnPlaceAdInActive.activeSelf) m_ClientBtnPlaceAdInActive.SetActive(true);
		}
	}
	#endregion //ボタン関係の関数群

	// --------
	#region レイキャスト関係の関数群
	/// <summary>
	/// オートドローン設置用レイキャスト
	/// </summary>
	protected void adPlaceRaycastUpdate()
	{
		if (!InitCompFlag) return;

		RaycastHit hit;

		if (Physics.Raycast(AdRayOriginTf.position, AdRayOriginTf.TransformDirection(Vector3.forward), out hit, m_AdRayDist, m_AdRayLayerMask))
		{
			AdPlaceMarkerTf.position = hit.point;
			if (AdPlaceMarkerCtrl.PlacePossible)
			{
				this.AdRayOriginCtrl.chgReticleActive(true);
				this.AdPlaceMarkerCtrl.showMarkerModel(true);
				this.chgBtnPlaceAdActive(true);
			}
			else
			{
				this.AdRayOriginCtrl.chgReticleActive(false);
				this.AdPlaceMarkerCtrl.showMarkerModel(false);
				this.chgBtnPlaceAdActive(false);
			}
		}
		else
		{
			this.AdRayOriginCtrl.chgReticleActive(false);
			this.AdPlaceMarkerCtrl.showMarkerModel(false);
			this.chgBtnPlaceAdActive(false);
		}
	}
	#endregion //レイキャスト関係の関数群

	// --------
	#region Ad設置関係
	/// <summary>
	/// Ad設置モードに移行する関数
	/// </summary>
	public void chgAdPlaceMode()
	{
		if (InitCompFlag)
		{
			if (!ProcessingFlag)
			{
				ProcessingFlag = true;
				m_CurrentMode = AdManagerModeState.PLACE;
				m_OnModeChangeEvent.Invoke(m_CurrentMode);
				ProcessingFlag = false;
			}
			else
			{
				//処理中のため
			}
		}
		else
		{
			//初期化していないため
		}
	}
	/// <summary>
	/// Adを設置する関数
	/// </summary>
	public void placeAd()
	{
		if (InitCompFlag)
		{
			if (!ProcessingFlag)
			{
				ProcessingFlag = true;
				GameObject obj = (GameObject)PhotonNetwork.Instantiate(
					m_MultiAdPrefabName,
					AdPlaceMarkerTf.position,
					AdPlaceMarkerCtrl.PivotTf.localRotation,
					0
				);
				obj.GetComponent<MultiAdController>().init();
				//TODO ステージの子にするか否か
				m_AdObjDict.Add(obj.GetComponent<MultiAdController>().PhotonViewId, obj); //Dictに格納
				m_OnPlacedOwnAutoDroneEvent.Invoke(obj.GetComponent<MultiAdController>().PhotonViewId); //生成したAdのViewIdを渡す

				//設置後、WAITにモードに移行
				m_CurrentMode = AdManagerModeState.WAIT;
				m_OnModeChangeEvent.Invoke(m_CurrentMode); //モードの値を送信
				ProcessingFlag = false;

			}
			else
			{
				//処理中のため
			}
		}
		else
		{
			//初期化していないため
		}
	}

	/// <summary>
	/// 他者のAdのViewIDを受信したとき
	/// </summary>
	public void onReceiveOtherAdViewId(int _otherAdViewId)
	{
		Debug.Log("madm- on-receive-other-ad-view-id _otherAdViewId :"+_otherAdViewId);
		PhotonView pv = PhotonView.Find(_otherAdViewId);
		m_AdObjDict.Add(pv.ViewID, pv.gameObject);
	}
	#endregion //Ad配置関係

	// --------
	#region メンバメソッド
	/// <summary>
	/// 待ち状態のモード
	/// </summary>
	public void chgWaitMode()
	{
		if(InitCompFlag)
		{
			if(!ProcessingFlag)
			{
				ProcessingFlag = true;
				//WAITモードに移行
				m_CurrentMode = AdManagerModeState.WAIT;
				m_OnModeChangeEvent.Invoke(m_CurrentMode);
				ProcessingFlag = false;
			}
			else
			{
				//処理中のため
			}
		}
		else
		{
			//初期化していないため
		}
	}
	/// <summary>
	/// 初期化処理
	/// </summary>
	public void init(OnComp<bool> callback)
	{
		bool res = false;
		if(!InitCompFlag)
		{
			m_AdRayOriginObj = (GameObject)Instantiate(m_AdRayOriginPrefab, Vector3.zero, Quaternion.identity);
			AdRayOriginTf.parent = m_Camera.transform;
			AdRayOriginTf.localPosition = Vector3.zero;
			AdRayOriginTf.localRotation = Quaternion.identity;
			AdRayOriginTf.localScale = Vector3.one;

			m_AdPlaceMarkerObj = (GameObject)Instantiate(m_AdPlaceMarkerPrefab, Vector3.zero, Quaternion.identity);

			res = InitCompFlag = true;
		}
		callback(res);
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
