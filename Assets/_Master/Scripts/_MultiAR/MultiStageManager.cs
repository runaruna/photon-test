﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// マルチプレイ用ステージマネージャー
/// </summary>
public class MultiStageManager : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	[Header("*Ray Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private Camera m_Camera;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private GameObject m_StageRayOriginObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform StageRayOriginTf 
	{
		get { return m_StageRayOriginObj.transform; }
	}
	/// <summary>
	/// レイのレンジ
	/// </summary>
	[SerializeField] private float m_RayDist = 50.0f;
	/// <summary>
	/// ステージ用レイキャストのレイヤーリスト
	/// </summary>
	[SerializeField] private string[] m_StageRayLayerList;
	/// <summary>
	/// ステージ用レイキャストのレイヤーマスク
	/// </summary>
	private int m_StageRayLayerMask;
	[Header("*StageObj Settings")]
	/// <summary>
	/// StageMarkerの親
	/// </summary>
	[SerializeField] private GameObject m_StageMarkerObj;
	/// <summary>
	/// [プロパティ]StageMarkerの親
	/// </summary>
	public Transform StageMarkerTf
	{
		get { return m_StageMarkerObj.transform; }
	}
	/// <summary>
	/// [プロパティ]MultiStagerMarkerController
	/// </summary>
	public MultiStageMarkerController StageMarkerCtrl 
	{
		get { return m_StageMarkerObj.GetComponent<MultiStageMarkerController>(); }
	}
	/// <summary>
	/// The stage prefab data table.
	/// </summary>
	[SerializeField] private StagePrefabDataTable m_StagePrefabDataTable;
	[Header("*UI: BtnPlaceStageInActive")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private GameObject m_PortraitBtnPlaceStageInActive;
	[Header("*UI: BtnPlaceStageActive")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] private GameObject m_PortraitBtnPlaceStageActive;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 処理中フラグ
	/// </summary>
	protected bool m_ProcessingFlag = false;
	/// <summary>
	/// [プロパティ] 処理中フラグ
	/// </summary>
	public bool ProcessingFlag
	{
		get { return m_ProcessingFlag; }
		private set { m_ProcessingFlag = value; }
	}
	/// <summary>
	/// レイキャストRunフラグ
	/// </summary>
	protected bool m_RaycastRunFlag;
	/// <summary>
	/// [プロパティ] レイキャストRunフラグ
	/// </summary>
	public bool RaycastRunFlag
	{
		get { return m_RaycastRunFlag; }
		protected set { m_RaycastRunFlag = value; }
	}
	/// <summary>
	/// ステージプレファブデータのKeyを格納するフィールド
	/// </summary>
	private string m_CurrentStagePrefabDataKey;
	/// <summary>
	/// The stage prefab data dictionary.
	/// </summary>
	private Dictionary<string, StagePrefabData> m_StagePrefabDataDict;
	/// <summary>
	/// ホロステージオブジェクト
	/// </summary>
	private GameObject m_HoloStageObj;
	/// <summary>
	/// ステージオブジェクトのviewid
	/// </summary>
	private int m_StageViewId;
	/// <summary>
	/// [プロパティ]ステージオブジェクトのviewid
	/// </summary>
	public int StageViewId
	{
		get { return StageViewId; }
		set { StageViewId = value; }
	}
	/// <summary>
	/// ステージオブジェクト
	/// </summary>
	private GameObject m_StageObj;
	/// <summary>
	/// [プロパティ] ステージオブジェクト
	/// </summary>
	public GameObject StageObj
	{
		get { return m_StageObj; }
		private set { m_StageObj = value; }
	}
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	#region UnityEvents
	[System.SerializableAttribute] public class CustomEventInt : UnityEvent<int> { }
	[Header("*ステージ設置時のイベント")]
	[SerializeField] public CustomEventInt m_OnPlaceStageEvent;
	#endregion //UnityEvents

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		m_StagePrefabDataDict = m_StagePrefabDataTable.GetTable();
		m_StageRayLayerMask = LayerMask.GetMask(m_StageRayLayerList);
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		StageRayOriginTf.parent = m_Camera.transform;
		StageRayOriginTf.localPosition = Vector3.zero;
		StageRayOriginTf.localRotation = Quaternion.identity;

		StageMarkerCtrl.showHoloStageHolder(false);

		m_CurrentStagePrefabDataKey = "REGULAR";

		m_HoloStageObj = (GameObject)Instantiate(
			m_StagePrefabDataDict[m_CurrentStagePrefabDataKey].m_HoloStagePrefab,
			Vector3.zero,
			Quaternion.identity
		);
		m_HoloStageObj.transform.parent = StageMarkerCtrl.HoloStageHolderTf;
		m_HoloStageObj.transform.localPosition = Vector3.zero;
		m_HoloStageObj.transform.localRotation = Quaternion.identity;
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{
		if(RaycastRunFlag)
		{
			StageMarkerCtrl.showHoloStageHolder(true);
			this.StageRaycastUpdate();
		}
		else
		{
			StageMarkerCtrl.showHoloStageHolder(false);
		}

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region レイキャスト関係
	/// <summary>
	/// レイキャストを開始する関数
	/// </summary>
	public void startStageRaycast()
	{
		RaycastRunFlag = true;
	}
	/// <summary>
	/// 
	/// </summary>
	private void StageRaycastUpdate()
	{
		RaycastHit hit;

		if(Physics.Raycast(StageRayOriginTf.position, StageRayOriginTf.TransformDirection(Vector3.forward), out hit, m_RayDist, m_StageRayLayerMask))
		{
			StageMarkerTf.position = hit.point;
			StageMarkerCtrl.showHoloStageHolder(true);
			m_PortraitBtnPlaceStageActive.SetActive(true);
		}
		else
		{
			m_PortraitBtnPlaceStageActive.SetActive(false);
			StageMarkerCtrl.showHoloStageHolder(false);
		}
	}
	#endregion //レイキャスト関係

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	public void placeStage()
	{
		if(!ProcessingFlag)
		{
			ProcessingFlag = true;
		}
		else
		{
			return;
		}

		StartCoroutine(_placeStage(()=>
		{
			m_OnPlaceStageEvent.Invoke(m_StageViewId);
			RaycastRunFlag = false;
			ProcessingFlag = false;
		}));
	}
	IEnumerator _placeStage(OnComplete callback)
	{
		m_StageObj = null;
		m_StageObj = PhotonNetwork.InstantiateSceneObject(
			m_StagePrefabDataDict[m_CurrentStagePrefabDataKey].m_StagePrefabName,
			StageMarkerTf.position,
			Quaternion.identity,
			0
		);
		yield return m_StageObj;

		m_StageViewId = m_StageObj.GetComponent<PhotonView>().ViewID;

		//ステージの向きを補正========================
		m_StageObj.transform.LookAt(new Vector3(
			m_Camera.transform.position.x,
			m_StageObj.transform.position.y,
			m_Camera.transform.position.z
		));
		//========================================

		callback();
		yield break;
	}
	/// <summary>
	/// photonviewのviewIdからstageObjを見つけてフィールドにセットする関数
	/// </summary>
	public void findSetStageObj(int _viewId, OnComplete callback)
	{
		StartCoroutine(_findSetStageObj(_viewId, ()=>
		{
			callback();
		}));
	}
	IEnumerator _findSetStageObj(int _viewId, OnComplete callback)
	{
		PhotonView pv = null;
		pv = PhotonView.Find(_viewId);
		yield return pv;
		m_StageObj = pv.gameObject;
		m_StageViewId = _viewId;

		callback();
		yield break;
	}
	#endregion //メンバメソッド

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	/// <summary>
	/// ステージプレファブデータ
	/// </summary>
	[System.SerializableAttribute]
	public class StagePrefabData
	{
		/// <summary>
		/// ホロステージプレファブ
		/// </summary>
		public GameObject m_HoloStagePrefab;
		/// <summary>
		/// ステージプレファブの名前
		/// </summary>
		public string m_StagePrefabName;

		//コンストラクタ
		public StagePrefabData()
		{
		}
	}

	/// <summary>
	/// ステージプレファブテーブル
	/// ジェネリックを隠すために継承してしまう
	/// [System.Serializable]を書くのを忘れない
	/// </summary>
	[System.Serializable]
	public class StagePrefabDataTable : Serialize.TableBase<string, StagePrefabData, StagePrefabDataPair>
	{

	}
	/// <summary>
	/// ジェネリックを隠すために継承してしまう
	/// [System.Serializable]を書くのを忘れない
	/// </summary>
	[System.Serializable]
	public class StagePrefabDataPair : Serialize.KeyAndValue<string, StagePrefabData>
	{
		public StagePrefabDataPair(string key, StagePrefabData value) : base(key, value)
		{
		}
	}
	#endregion //インナークラス
}
