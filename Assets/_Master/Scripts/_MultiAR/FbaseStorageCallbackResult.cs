﻿using System.Collections;
using System.Collections.Generic;

public class FbaseStorageCallbackResult : BaseCallbackResult
{
	// --------
	#region メンバフィールド
	/// <summary>
	/// シリアライズされたARWorldMapのデータ
	/// </summary>
	public byte[] serializedWMapData;
	#endregion //メンバフィールド

	// --------
	#region コンストラクタ
	/// <summary>
	/// コンストラクタ
	/// </summary>
	public FbaseStorageCallbackResult():base()
	{
	}
	#endregion //コンストラクタ
}
