﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using Firebase.Storage;

/// <summary>
/// 
/// </summary>
public class FbaseStorageManager : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// FirebaseStoregeのリファレンスのURI
	/// </summary>
	[SerializeField] protected string m_ReferenceUri = "gs://xxxx-yyyy.appspot.com";
	/// <summary>
	/// FirebaseStoregeにあるWorldMapを格納するフォルダの名前
	/// </summary>
	[SerializeField] protected string m_WorldMapDirectoryName = "arworldmaps";
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// FirebaseStoregeのインスタンスを格納するフィールド
	/// </summary>
	protected FirebaseStorage m_FbaseStorage;
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		m_FbaseStorage = Firebase.Storage.FirebaseStorage.DefaultInstance;
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region アップロード
	/// <summary>
	/// FirebaseStorage上に指定のファイル名でbyte型配列でシリアライズされたARWorldMapをアップロードする
	/// </summary>
	/// <param name="_fname"></param>
	/// <param name="_serializedWMap"></param>
	/// <param name="callback"></param>
	public void uploadSerializedARWorldMap(string _fname, byte[] _serializedWMap, OnComp<FbaseStorageCallbackResult> callback)
	{
		Debug.Log("mob- fsm- up- start...");
		FbaseStorageCallbackResult res = new FbaseStorageCallbackResult();

		string upUri = m_ReferenceUri + "/" + m_WorldMapDirectoryName + "/" + _fname;
		StorageReference upRef = m_FbaseStorage.GetReferenceFromUrl(upUri);

		Task<StorageMetadata> task = upRef.PutBytesAsync(_serializedWMap, null, new StorageProgress<UploadState>(state =>
		{
			// called periodically during the upload
			Debug.Log(string.Format(
				"mod- fsm- up- Progress: {0} of {1} bytes transferred.",
				state.BytesTransferred,
				state.TotalByteCount
			));
		}), CancellationToken.None, null);
		task.ContinueWith(resTask =>
		{
			if (resTask.IsFaulted || resTask.IsCanceled)
			{
				Debug.Log(resTask.Exception.ToString());
				res.result = false;
				res.errorMessage = resTask.Exception.ToString();
				callback(res);
			}
			else
			{
				StorageMetadata metadata = task.Result;
				Debug.Log("mob- fsm- up- Finished uploading!");
				res.result = true;
				callback(res);
			}
		});
	}
	#endregion //アップロード

	// --------
	#region ダウンロード
	/// <summary>
	/// FirebaseStorage上にあるARWorldMapをbyte型配列でダウンロードする
	/// </summary>
	/// <param name="_fname"></param>
	/// <param name="callback"></param>
	public void downloadSerializedARWorldMap(string _fname, OnComp<FbaseStorageCallbackResult> callback)
	{
		Debug.Log("mob- fsm- dl- start...");
		FbaseStorageCallbackResult res = new FbaseStorageCallbackResult();

		string dlUri = m_ReferenceUri + "/" + m_WorldMapDirectoryName + "/" + _fname;
		StorageReference dlRef = m_FbaseStorage.GetReferenceFromUrl(dlUri);
		long maxAllowedSize = 10 * 1024 * 1024; //10MBまでのデータだけダウンロードできるように制約をかける

		Task<byte[]> task = dlRef.GetBytesAsync(maxAllowedSize, new StorageProgress<DownloadState>((DownloadState state) =>
		{
			Debug.Log(string.Format(
				"mob- fsm- dl- Progress: {0} of {1} bytes transferred.",
				state.BytesTransferred,
				state.TotalByteCount
			));
		}), CancellationToken.None);
		task.ContinueWith(resTask =>
		{
			if (resTask.IsFaulted || resTask.IsCanceled)
			{
				Debug.Log(resTask.Exception.ToString());
				res.result = false;
				res.errorMessage = resTask.Exception.ToString();
				callback(res);
			}
			else
			{
				byte[] fileContents = task.Result;
				Debug.Log("mob- fsm- dl- Finished downloading!");
				Debug.Log("mob- fsm- dl- fileContents.length : " + fileContents.Length);
				res.result = true;
				res.serializedWMapData = fileContents;
				callback(res);
			}
		});
	}
	#endregion //ダウンロード

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
