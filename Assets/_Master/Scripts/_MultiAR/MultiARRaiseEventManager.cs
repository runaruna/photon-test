﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// マルチARシーン用RaiseEventManager
/// </summary>
public class MultiARRaiseEventManager : BaseRaiseEventManager
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	#endregion //delegate Type Member Fields

	// --------
	#region UnityEvents
	[System.SerializableAttribute] public class CustomEventInt : UnityEvent<int> { }
	[System.SerializableAttribute] public class CustomEventStr : UnityEvent<string> { }
	[Header("*MCのモードステータスが変更されたときのイベント")]
	[SerializeField] public CustomEventInt m_OnChgMCModeStateEvent;
	[Header("*ワールドマップアップロード完了メッセージ受信時のイベント")]
	[SerializeField] public CustomEventStr m_OnReceiveWMapUploadedCompMessageEvent;
	[Header("*ワールドマップの読み込み同期完了メッセージ受信時のイベント")]
	[SerializeField] public CustomEventInt m_OnReceiveWMapLoadSyncCompMessageEvent;
	[Header("*ステージOBJのViewId受信時のイベント")]
	[SerializeField] public CustomEventInt m_OnReceiveStegeViewIdEvent;
	[Header("*オートドローンOBJのViewId受信時のイベント")]
	[SerializeField] public CustomEventInt m_OnReceiveAdViewIdEvent;
	#endregion //UnityEvents

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected override void Awake()
	{
		base.Awake();
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected override void Start ()
	{
		base.Start();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void Update ()
	{
		base.Update();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void LateUpdate()
	{
		base.LateUpdate();
	}
	/// <summary>
	/// 
	/// </summary>
	protected override void OnEnable()
	{
		Debug.Log("mob- OnEnable");
		PhotonNetwork.NetworkingClient.EventReceived += OnEvent; //RaiseEventの登録
		InitCompFlag = true;
	}
	/// <summary>
	/// 
	/// </summary>
	protected override void OnDisable()
	{
		Debug.Log("mob- OnDisable");
		PhotonNetwork.NetworkingClient.EventReceived -= OnEvent; //RaiseEventの破棄
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region RaiseEvent
	/// <summary>
	/// RaiseEvent
	/// </summary>
	protected override void OnEvent(EventData _photonEvent)
	{
		switch (_photonEvent.Code)
		{
			case MultiARRaiseEventCodes.SendTest:
				Debug.Log("mob- marrem- oe- SendTest : " + (string)_photonEvent.CustomData);
			break;
			case MultiARRaiseEventCodes.ChgMCModeState:
				Debug.Log("mob- marrem- oe- ChgMCModeState : "+(int)_photonEvent.CustomData);
				m_OnChgMCModeStateEvent.Invoke((int)_photonEvent.CustomData); //変更されたMCのモードステータスを返す
			break;
			case MultiARRaiseEventCodes.WorldMapUploaded:
				Debug.Log("mob- marrem- oe- WorldMapUploaded");
				m_OnReceiveWMapUploadedCompMessageEvent.Invoke((string)_photonEvent.CustomData);　//アップロードしたワールドマップのファイル名を返す
			break;
			case MultiARRaiseEventCodes.WorldMapLoadSyncCompleted:
				Debug.Log("mob- marrem- oe- WorldMapLoadSyncCompleted");
				m_OnReceiveWMapLoadSyncCompMessageEvent.Invoke(_photonEvent.Sender); //プレイヤーのアクター番号を返す
			break;
			case MultiARRaiseEventCodes.ReceiveStageViewId:
				Debug.Log("mob- marrem- oe- ReceiveStageViewId");
				m_OnReceiveStegeViewIdEvent.Invoke((int)_photonEvent.CustomData); //ステージオブジェクトのViewIdを返す
			break;
			default:
			break;
		}
		base.OnEvent(_photonEvent);
	}
	/// <summary>
	/// RaiseEvent送信テスト
	/// </summary>
	public override void sendRaiseEventMessageTest()
	{
		base.sendRaiseEventMessageTest();
	}
	/// <summary>
	/// [MC用]マスタークライアントのモードステータスを送る関数
	/// </summary>
	public void sendMCModeState(int _modeState)
	{
		var raiseEventOptions = new RaiseEventOptions()
		{
			Receivers = ReceiverGroup.Others,
			CachingOption = EventCaching.DoNotCache
		};
		PhotonNetwork.RaiseEvent(
			MultiARRaiseEventCodes.ChgMCModeState,
			_modeState,
			raiseEventOptions,
			SendOptions.SendReliable
		);
	}
	/// <summary>
	/// アップロードしたワールドマップのファイル名を自分以外のクライアントへ送る関数
	/// (IENEKO-XXXXXXXX.worldmap)
	/// *マスタークライアントが使う関数
	/// </summary>
	/// <param name="_path"></param>
	public void sendUploadedWorldMapFileName(string _fname)
	{
		var raiseEventOptions = new RaiseEventOptions()
		{
			Receivers = ReceiverGroup.Others,
			CachingOption = EventCaching.DoNotCache
		};
		PhotonNetwork.RaiseEvent(
			MultiARRaiseEventCodes.WorldMapUploaded,
			_fname,
			raiseEventOptions,
			SendOptions.SendReliable
		);
	}
	/// <summary>
	/// ワールドマップの読み込み同期が完了したときにマスタークライアントへ完了ことを伝える関数
	/// *マスタークライアント以外のクライアントが使う関数
	/// </summary>
	public void sendLoadSyncCompMessage()
	{
		var raiseEventOptions = new RaiseEventOptions()
		{
			Receivers = ReceiverGroup.MasterClient,
			CachingOption = EventCaching.DoNotCache
		};
		PhotonNetwork.RaiseEvent(
			MultiARRaiseEventCodes.WorldMapLoadSyncCompleted,
			"LoadSyncComp",
			raiseEventOptions,
			SendOptions.SendReliable
		);
	}
	/// <summary>
	/// ステージオブジェクトのviewidをMC以外のクライエントへ送る
	/// </summary>
	/// <param name="_stageViewId"></param>
	public void sendStageViewId(int _stageViewId)
	{
		var raiseEventOptions = new RaiseEventOptions()
		{
			Receivers = ReceiverGroup.Others,
			CachingOption = EventCaching.DoNotCache
		};
		PhotonNetwork.RaiseEvent(
			MultiARRaiseEventCodes.ReceiveStageViewId,
			_stageViewId,
			raiseEventOptions,
			SendOptions.SendReliable
		);
	}
	/// <summary>
	/// オートドローンのviewidを他のクライエントへ送る
	/// </summary>
	/// <param name="_AdViewId"></param>
	public void sendAdViewId(int _AdViewId)
	{
		var raiseEventOptions = new RaiseEventOptions()
		{
			Receivers = ReceiverGroup.Others,
			CachingOption =EventCaching.DoNotCache
		};
		PhotonNetwork.RaiseEvent(
			MultiARRaiseEventCodes.ReceiveAdViewId,
			_AdViewId,
			raiseEventOptions,
			SendOptions.SendReliable
		);
	}
	#endregion //RaiseEvent

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
