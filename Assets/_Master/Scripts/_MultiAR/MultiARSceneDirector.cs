﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;

public class MultiARSceneDirector : BasePunSceneDirector
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	[Header("*RaiseEventManager Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected MultiARRaiseEventManager m_RaiseEventManager;
	[Header("*AR Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected CustomUnityARCameraManager m_ARCameraManager;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected CustomUnityARGeneratePlane m_ARGeneratePlane; 
	[Header("*FbaseStoreageManager Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected FbaseStorageManager m_FbaseStorageManager;
	[Header("*StageManager Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected MultiStageManager m_StageManager;
	[Header("*AdManager Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected MultiAdManager m_AdManager;


	[Header("*UI MasterClientUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitMCUIGroup;
	[Header("*UI MasterClientInitUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitMCInitUIGroup;
	[Header("*UI MasterClientCreateWMapUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitMCCreateWMapUIGroup;
	[Header("*UI MasterClientSendWMapUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitMCSendWMapUIGroup;
	[Header("*UI MasterClientWaitAllPlayerLoadSyncCompUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitMCWaitAllPlayerLoadSyncCompUIGroup;
	[Header("*UI OtherPlyerCntText")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Text m_PortraitOtherPlyerCntText;
	[Header("*UI WMapLoadSyncCompPlayerCntText")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Text m_PortraitWMapLoadSyncCompPlayerCntText;
	[Header("*UI MasterClientPlaceStageUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitMCPlaceStageUIGroup;
	[Header("*UI MasterClientPlacePetUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitMCPlacePetUIGroup;
	[Header("*UI MasterClientSetupCompUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitMCSetupCompUIGroup;

	[Header("*UI ClientUIGroup")]
		/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitCUIGroup;
	[Header("*UI ClientInitUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitCInitUIGroup;
	[Header("*UI ClientWaitReceiveWMapUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitCWaitReceiveWMapUIGroup;
	[Header("*UI ClientReceiveWMapUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitCReceiveWMapUIGroup;
	[Header("*UI ClientLoadSyncWMapUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitCLoadSyncWMapUIGroup;
	[Header("*UI ClientWaitPlaceStageCompUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitCWaitPlaceStageCompUIGroup;
	[Header("*UI ClientPlacePetUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitCPlacePetUIGroup;
	[Header("*UI ClientSetupCompUIGroup")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PortraitCSetupCompUIGroup;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// [MC用]マスタークライアントの現状のステータスを格納するフィールド
	/// </summary>
	protected int m_CurrentMCModeState;
	/// <summary>
	/// [プロパティ][MC用]マスタークライアントの現状のステータスを格納するフィールド
	/// </summary>
	public int CurrentMCModeState 
	{
		get { return m_CurrentMCModeState; }
		protected set { m_CurrentMCModeState = value; }
	}
	/// <summary>
	/// [C用]クライアントの現状のステータスを格納するフィールド
	/// </summary>
	protected int m_CurrentCModeState;
	/// <summary>
	/// [プロパティ][C用]クライアントの現状のステータスを格納するフィールド
	/// </summary>
	public int CurrentCModeState 
	{
		get { return m_CurrentCModeState; }
		protected set { m_CurrentCModeState = value; }
	}
	/// <summary>
	/// [MC用]自分以外のプレイヤーの数
	/// </summary>
	protected int m_OtherPlayerCnt;
	/// <summary>
	/// [MC用]ワールドマップの読み込み同期が完了したプレイヤーの数
	/// </summary>
	protected int m_WMapLoadSyncCompPlayerCnt;
	/// <summary>
	/// プレイヤーがJoinしている部屋名
	/// </summary>
	protected string m_CurrentRoomName;
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected override void Awake()
	{
		base.Awake();
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected override void Start ()
	{
		base.Start();
		m_OtherPlayerCnt = PhotonNetwork.PlayerListOthers.Length;
		m_CurrentRoomName = PhotonNetwork.CurrentRoom.Name;
		m_AdManager.init((res)=>{});
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void Update ()
	{
		base.Update();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void LateUpdate()
	{
		m_OtherPlayerCnt = PhotonNetwork.PlayerListOthers.Length; //自分以外のプレイヤーの数を更新
		this.GameLifeCycleUpdate();
		base.LateUpdate();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region PUN コールバック
	/// <summary>
	/// PhotonNetwork.autoJoinLobby が false の状態で、
	/// マスターサーバーへ接続および認証が完了した際に呼び出されます。
	/// </summary>
	public override void OnConnectedToMaster()
	{
		base.OnConnectedToMaster();
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="cause"></param>
	public override void OnDisconnected(DisconnectCause cause)
	{
		base.OnDisconnected(cause);

		m_ARCameraManager.reset();
		translateScenePortrait("Lobby");
	}
	/// <summary>
	/// Master Serverのロビーに入るときに呼び出されます
	/// </summary>
	public override void OnJoinedLobby()
	{
		base.OnJoinedLobby();
	}
	/// <summary>
	/// ロビーを退出した際に呼び出されます。
	/// </summary>
	public override void OnLeftLobby()
	{
		base.OnLeftLobby();
	}
	/// <summary>
	/// Master Serverのロビー（InLobby）にいる間に、ルームリストが更新されるたびに呼び出されます。
	/// </summary>
	/// <param name="roomList"></param>
	public override void OnRoomListUpdate(List<RoomInfo> roomList)
	{
		base.OnRoomListUpdate(roomList);
	}
	/// <summary>
	/// ルームの作成に成功したときに呼び出される
	/// </summary>
	public override void OnCreatedRoom()
	{
		base.OnCreatedRoom();
	}
	/// <summary>
	/// ルームの作成に失敗したときに呼び出される
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnCreateRoomFailed(short returnCode, string message)
	{
		base.OnCreateRoomFailed(returnCode, message);
	}
	/// <summary>
	/// ルームの入室に失敗したときに呼び出される
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRoomFailed(short returnCode, string message)
	{
		base.OnJoinRandomFailed(returnCode, message);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		base.OnJoinRandomFailed(returnCode, message);
	}
	/// <summary>
	/// 部屋に入室した際に呼ばれる
	/// </summary>
	public override void OnJoinedRoom()
	{
		base.OnJoinedRoom();
	}
	/// <summary>
	/// 部屋から退出した際に呼ばれる
	/// </summary>
	public override void OnLeftRoom()
	{
		base.OnLeftRoom();
		PhotonNetwork.Disconnect();
	}
	/// <summary>
	/// リモートプレイヤーが部屋に入室した際に呼ばれる
	/// (このプレーヤーは既にプレーヤーリストに追加されています)
	/// </summary>
	/// <param name="newPlayer"></param>
	public override void OnPlayerEnteredRoom(Player newPlayer)
	{
		m_OtherPlayerCnt = PhotonNetwork.PlayerListOthers.Length; //自分以外のプレイヤー数を更新
		base.OnPlayerEnteredRoom(newPlayer);
	}
	/// <summary>
	/// リモートプレイヤーが部屋を離れるか、非アクティブになったときに呼び出されます。
	/// </summary>
	/// <param name="otherPlayer"></param>
	public override void OnPlayerLeftRoom(Player otherPlayer)
	{
		m_OtherPlayerCnt = PhotonNetwork.PlayerListOthers.Length; //自分以外のプレイヤー数を更新
		base.OnPlayerLeftRoom(otherPlayer);
	}
	/// <summary>
	/// 現在のMasterClientが終了したときに新しいMasterClientに切り替えた後に呼び出されます。
	/// </summary>
	/// <param name="newMasterClient"></param>
	public override void OnMasterClientSwitched(Player newMasterClient)
	{
		base.OnMasterClientSwitched(newMasterClient);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="target"></param>
	/// <param name="changedProps"></param>
	public override void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
	{
		base.OnPlayerPropertiesUpdate(target, changedProps);
	}
	#endregion //PUN コールバック

	// --------
	#region ログアウト
	/// <summary>
	/// ログアウト
	/// </summary>
	public void disconnect()
	{
		if (PhotonNetwork.IsMasterClient)
		{
			//TODO 全プレイヤーを強制的にログアウトするようにメッセージを送る
		}
		PhotonNetwork.Disconnect();
	}
	#endregion //ログアウト

	// --------
	#region ARカメラ関係
	/// <summary>
	/// [MC用]ワールド作成モード移行関数
	/// </summary>
	public void createWMapMode()
	{
		this.chgMCModeState(MCModeState.CreateWMap);
		m_ARCameraManager.chgCreateWorldMapMode(); //ARカメラをワールドマップ作成モードに変更
	}
	/// <summary>
	/// [MC用]作成したワールドマップをSave時のコールバック
	/// </summary>
	/// <param name="_serializedWMap"></param>
	public void onSavedSerializedWorldMap(byte[] _serializedWMap)
	{
		Debug.Log("mob- marsd- onSavedSerializedWorldMap");

		//FirebaseStorageにアップロードする
		this.chgMCModeState(MCModeState.SendWMap);

		string fname = m_CurrentRoomName + ".worldmap";
		m_FbaseStorageManager.uploadSerializedARWorldMap(fname, _serializedWMap, (FbaseStorageCallbackResult res) =>
		{
			if(res.result)
			{
				this.chgMCModeState(MCModeState.WaitAllPlayerWMapLoadSyncComp);
				m_RaiseEventManager.sendUploadedWorldMapFileName(fname);
			}
			else
			{
//TODOリトライさせる
Debug.Log("mob- res.errorMessage : "+res.errorMessage);
			}
		});
	}
	/// <summary>
	/// [C用]読み込んだワールドマップの同期が完了時のコールバック
	/// </summary>
	/// <param name="_result"></param>
	public void onRelocalizedEvent(bool _result)
	{
		Debug.Log("mob- marsd- onRelocalizedEvent");
		CurrentCModeState = CModeState.WaitPlaceStageComp;
		m_RaiseEventManager.sendLoadSyncCompMessage(); //マスタークライアントに読み込み同期したことを伝える
	}
	#endregion //ARカメラ関係

	// --------
	#region ステージマネージャ関係
	/// <summary>
	/// [MC用]ステージ配置時
	/// </summary>
	/// <param name="_stageViewId"></param>
	public void onPlaceStage(int _stageViewId)
	{
		Debug.Log("mob- marsd- onplacestage- _stageViewId :"+_stageViewId);
		m_ARGeneratePlane.stopGeneratePlane();
		this.chgMCModeState(MCModeState.PlacePet);
		m_RaiseEventManager.sendStageViewId(_stageViewId);
		m_AdManager.chgAdPlaceMode();
	}
	/// <summary>
	/// [C用]ステージOBJのViewID受信時
	/// </summary>
	public void onReceiveStageViewId(int _stageViewId)
	{
		Debug.Log("mob- marsd- onReceiveStageViewId- _stageViewId :" + _stageViewId);
		m_StageManager.findSetStageObj(_stageViewId, () =>
		{
			m_ARGeneratePlane.stopGeneratePlane();
			m_CurrentCModeState = CModeState.PlacePet;
			m_AdManager.chgAdPlaceMode();
		});
	}
	#endregion //ステージマネージャ関係

	// --------
	#region Ad(オートドローン)マネージャ関係
	/// <summary>
	/// [MC・C共用]自身のAdを配置した時
	/// </summary>
	/// <param name="_adViewId"></param>
	public void onPlaceOwnAd(int _adViewId)
	{
		Debug.Log("mob- marsd- on-placed-own-ad- _adViewId : "+_adViewId);
		if (PhotonNetwork.IsMasterClient)
		{
			m_CurrentMCModeState = MCModeState.SetupComp;
			m_RaiseEventManager.sendMCModeState(m_CurrentMCModeState);
		}
		else
		{
			m_CurrentCModeState = CModeState.SetupComp;
		}
		m_RaiseEventManager.sendAdViewId(_adViewId);
	}
	/// <summary>
	/// [MC・C共用]他のクライアントのAdが出現した時
	/// </summary>
	/// <param name="_adViewId"></param>
	public void onReceiveOtherAdViewId(int _adViewId)
	{
		m_AdManager.onReceiveOtherAdViewId(_adViewId);
	}
	#endregion //Adマネージャ関係

	// --------
	#region RaiseEvent関係
	/// <summary>
	/// [C用] MCの変更されたモードステータスを受け取る関数
	/// </summary>
	public void onChgMCModeState(int _newModeState)
	{
		CurrentMCModeState = _newModeState;
		Debug.Log("mob- CurrentMCModeState:"+CurrentMCModeState);
	}
	/// <summary>
	/// [MC用] 各クライアントがワールドマップの読み込み同期が完了したときに完了したクライアントのアクター番号を受け取る関数
	/// </summary>
	/// <param name="_actorNumber">読み込み同期が完了したプレイヤーのアクター番号</param>
	public void onReceiveWMapLoadSyncCompMessage(int _actorNumber)
	{
		m_WMapLoadSyncCompPlayerCnt++;
	}
	/// <summary>
	/// [C用]マスタークライアントがアップロードしたワールドマップのファイル名を受け取る関数
	/// </summary>
	public void onReceiveDownloadWMapFileName(string _fname)
	{
		CurrentCModeState = CModeState.ReceiveWMap;
		m_FbaseStorageManager.downloadSerializedARWorldMap(_fname, (FbaseStorageCallbackResult res)=>
		{
			if(res.result)
			{
				Debug.Log("ダウンロード完了");
				CurrentCModeState = CModeState.LoadSyncWMap;
				m_ARCameraManager.loadSelializedWorldMap(res.serializedWMapData);
			}
			else
			{
				//TODO 再ダウンロード発動
			}

		});
	}
	#endregion //RaiseEvent関係

	// --------
	#region GameLifeCycle
	/// <summary>
	/// ゲームのLifeCycleの更新処理
	/// </summary>
	protected void GameLifeCycleUpdate()
	{
		if(!m_RaiseEventManager.InitCompFlag)
		{
			Debug.Log("mob- not init comp...");
			return;
		}


		if(PhotonNetwork.IsMasterClient)
		{
			
			/**
			* MC用ライフサイクル
			*/
			switch (CurrentMCModeState)
			{
				case MCModeState.Init:
				break;
				case MCModeState.CreateWMap:
				break;
				case MCModeState.SendWMap:
				break;
				case MCModeState.WaitAllPlayerWMapLoadSyncComp:
					this.chkLoadSyncCompOtherPlayersUpdate();
				break;
				case MCModeState.PlaceStage:
				break;
				case MCModeState.PlacePet:
				break;
				case MCModeState.SetupComp:
				break;
				default:
				break;
			}

			this.showMCUIGroup(true);
			this.showCUIGroup(false);
			this.chgMCUI(CurrentMCModeState);
		}
		else
		{
			/**
			* C用ライフサイクル
			*/
			switch (CurrentCModeState)
			{
				case CModeState.Init:
					CurrentCModeState = CModeState.WaitReceiveWMap;
				break;
				case CModeState.WaitReceiveWMap:
				break;
				case CModeState.ReceiveWMap:
				break;
				case CModeState.LoadSyncWMap:
				break;
				case CModeState.WaitPlaceStageComp:
				break;
				case CModeState.PlacePet:
				break;
				case CModeState.SetupComp:
				break;
				default:
				break;
			}

			this.showMCUIGroup(false);
			this.showCUIGroup(true);
			this.chgCUI(CurrentCModeState);
		}
	}
	#endregion //LifeCycle

	// --------
	#region UI関係
	/// <summary>
	/// 
	/// </summary>
	/// <param name="_flg"></param>
	protected void showMCUIGroup(bool _flg)
	{
		m_PortraitMCUIGroup.SetActive(_flg);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="_mcModeState"></param>
	protected void chgMCUI(int _mcModeState)
	{
		switch (_mcModeState)
		{
			case MCModeState.Init:
				m_PortraitMCInitUIGroup.SetActive(true);
				m_PortraitMCCreateWMapUIGroup.SetActive(false);
				m_PortraitMCSendWMapUIGroup.SetActive(false);
				m_PortraitMCWaitAllPlayerLoadSyncCompUIGroup.SetActive(false);
				m_PortraitMCPlaceStageUIGroup.SetActive(false);
				m_PortraitMCPlacePetUIGroup.SetActive(false);
				m_PortraitMCSetupCompUIGroup.SetActive(false);
			break;
			case MCModeState.CreateWMap:
				m_PortraitMCInitUIGroup.SetActive(false);
				m_PortraitMCCreateWMapUIGroup.SetActive(true);
				m_PortraitMCSendWMapUIGroup.SetActive(false);
				m_PortraitMCWaitAllPlayerLoadSyncCompUIGroup.SetActive(false);
				m_PortraitMCPlaceStageUIGroup.SetActive(false);
				m_PortraitMCPlacePetUIGroup.SetActive(false);
				m_PortraitMCSetupCompUIGroup.SetActive(false);
			break;
			case MCModeState.SendWMap:
				m_PortraitMCInitUIGroup.SetActive(false);
				m_PortraitMCCreateWMapUIGroup.SetActive(false);
				m_PortraitMCSendWMapUIGroup.SetActive(true);
				m_PortraitMCWaitAllPlayerLoadSyncCompUIGroup.SetActive(false);
				m_PortraitMCPlaceStageUIGroup.SetActive(false);
				m_PortraitMCPlacePetUIGroup.SetActive(false);
				m_PortraitMCSetupCompUIGroup.SetActive(false);
			break;
			case MCModeState.WaitAllPlayerWMapLoadSyncComp:
				m_PortraitMCInitUIGroup.SetActive(false);
				m_PortraitMCCreateWMapUIGroup.SetActive(false);
				m_PortraitMCSendWMapUIGroup.SetActive(false);
				m_PortraitMCWaitAllPlayerLoadSyncCompUIGroup.SetActive(true);
				m_PortraitMCPlaceStageUIGroup.SetActive(false);
				m_PortraitMCPlacePetUIGroup.SetActive(false);
				m_PortraitMCSetupCompUIGroup.SetActive(false);
			break;
			case MCModeState.PlaceStage:
				m_PortraitMCInitUIGroup.SetActive(false);
				m_PortraitMCCreateWMapUIGroup.SetActive(false);
				m_PortraitMCSendWMapUIGroup.SetActive(false);
				m_PortraitMCWaitAllPlayerLoadSyncCompUIGroup.SetActive(false);
				m_PortraitMCPlaceStageUIGroup.SetActive(true);
				m_PortraitMCPlacePetUIGroup.SetActive(false);
				m_PortraitMCSetupCompUIGroup.SetActive(false);
			break;
			case MCModeState.PlacePet:
				m_PortraitMCInitUIGroup.SetActive(false);
				m_PortraitMCCreateWMapUIGroup.SetActive(false);
				m_PortraitMCSendWMapUIGroup.SetActive(false);
				m_PortraitMCWaitAllPlayerLoadSyncCompUIGroup.SetActive(false);
				m_PortraitMCPlaceStageUIGroup.SetActive(false);
				m_PortraitMCPlacePetUIGroup.SetActive(true);
				m_PortraitMCSetupCompUIGroup.SetActive(false);
			break;
			case MCModeState.SetupComp:
				m_PortraitMCInitUIGroup.SetActive(false);
				m_PortraitMCCreateWMapUIGroup.SetActive(false);
				m_PortraitMCSendWMapUIGroup.SetActive(false);
				m_PortraitMCWaitAllPlayerLoadSyncCompUIGroup.SetActive(false);
				m_PortraitMCPlaceStageUIGroup.SetActive(false);
				m_PortraitMCPlacePetUIGroup.SetActive(false);
				m_PortraitMCSetupCompUIGroup.SetActive(true);
			break;
			default:
			break;
		}
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="_flg"></param>
	protected void showCUIGroup(bool _flg)
	{
		m_PortraitCUIGroup.SetActive(_flg);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="_cModeState"></param>
	protected void chgCUI(int _cModeState)
	{
		switch (CurrentCModeState)
		{
			case CModeState.Init:
				m_PortraitCInitUIGroup.SetActive(true);
				m_PortraitCWaitReceiveWMapUIGroup.SetActive(false);
				m_PortraitCReceiveWMapUIGroup.SetActive(false);
				m_PortraitCLoadSyncWMapUIGroup.SetActive(false);
				m_PortraitCWaitPlaceStageCompUIGroup.SetActive(false);
				m_PortraitCPlacePetUIGroup.SetActive(false);
				m_PortraitCSetupCompUIGroup.SetActive(false);
			break;
			case CModeState.WaitReceiveWMap:
				m_PortraitCInitUIGroup.SetActive(false);
				m_PortraitCWaitReceiveWMapUIGroup.SetActive(true);
				m_PortraitCReceiveWMapUIGroup.SetActive(false);
				m_PortraitCLoadSyncWMapUIGroup.SetActive(false);
				m_PortraitCWaitPlaceStageCompUIGroup.SetActive(false);
				m_PortraitCPlacePetUIGroup.SetActive(false);
				m_PortraitCSetupCompUIGroup.SetActive(false);
			break;
			case CModeState.ReceiveWMap:
				m_PortraitCInitUIGroup.SetActive(false);
				m_PortraitCWaitReceiveWMapUIGroup.SetActive(false);
				m_PortraitCReceiveWMapUIGroup.SetActive(true);
				m_PortraitCLoadSyncWMapUIGroup.SetActive(false);
				m_PortraitCWaitPlaceStageCompUIGroup.SetActive(false);
				m_PortraitCPlacePetUIGroup.SetActive(false);
				m_PortraitCSetupCompUIGroup.SetActive(false);
			break;
			case CModeState.LoadSyncWMap:
				m_PortraitCInitUIGroup.SetActive(false);
				m_PortraitCWaitReceiveWMapUIGroup.SetActive(false);
				m_PortraitCReceiveWMapUIGroup.SetActive(false);
				m_PortraitCLoadSyncWMapUIGroup.SetActive(true);
				m_PortraitCWaitPlaceStageCompUIGroup.SetActive(false);
				m_PortraitCPlacePetUIGroup.SetActive(false);
				m_PortraitCSetupCompUIGroup.SetActive(false);
			break;
			case CModeState.WaitPlaceStageComp:
				m_PortraitCInitUIGroup.SetActive(false);
				m_PortraitCWaitReceiveWMapUIGroup.SetActive(false);
				m_PortraitCReceiveWMapUIGroup.SetActive(false);
				m_PortraitCLoadSyncWMapUIGroup.SetActive(false);
				m_PortraitCWaitPlaceStageCompUIGroup.SetActive(true);
				m_PortraitCPlacePetUIGroup.SetActive(false);
				m_PortraitCSetupCompUIGroup.SetActive(false);
			break;
			case CModeState.PlacePet:
				m_PortraitCInitUIGroup.SetActive(false);
				m_PortraitCWaitReceiveWMapUIGroup.SetActive(false);
				m_PortraitCReceiveWMapUIGroup.SetActive(false);
				m_PortraitCLoadSyncWMapUIGroup.SetActive(false);
				m_PortraitCWaitPlaceStageCompUIGroup.SetActive(false);
				m_PortraitCPlacePetUIGroup.SetActive(true);
				m_PortraitCSetupCompUIGroup.SetActive(false);
			break;
			case CModeState.SetupComp:
				m_PortraitCInitUIGroup.SetActive(false);
				m_PortraitCWaitReceiveWMapUIGroup.SetActive(false);
				m_PortraitCReceiveWMapUIGroup.SetActive(false);
				m_PortraitCLoadSyncWMapUIGroup.SetActive(false);
				m_PortraitCWaitPlaceStageCompUIGroup.SetActive(false);
				m_PortraitCPlacePetUIGroup.SetActive(false);
				m_PortraitCSetupCompUIGroup.SetActive(true);
			break;
			default:
			break;
		}
	}
	#endregion //UI関係

	// --------
	#region メンバメソッド
	/// <summary>
	/// [MC用]マスタークライアントのモードステータスを変更する関数
	/// </summary>
	protected void chgMCModeState(int _mcModeState)
	{
		CurrentMCModeState = _mcModeState;
		m_RaiseEventManager.sendMCModeState(CurrentMCModeState);
	}
	/// <summary>
	/// [MC用] ワールドマップの読み込み同期が完了したプレイヤーの数をチェックする関数
	/// </summary>
	protected void chkLoadSyncCompOtherPlayersUpdate()
	{
		m_PortraitWMapLoadSyncCompPlayerCntText.text = m_WMapLoadSyncCompPlayerCnt.ToString();
		m_PortraitOtherPlyerCntText.text = m_OtherPlayerCnt.ToString();

		if(m_WMapLoadSyncCompPlayerCnt >= m_OtherPlayerCnt)
		{
			//全プレイヤー読み込み同期完了
			this.chgMCModeState(MCModeState.PlaceStage);
			m_StageManager.startStageRaycast(); //レイキャストの処理をスタートする
		}
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	/// <summary>
	/// マスタークライアント用モードステータスクラス
	/// </summary>
	public class MCModeState 
	{
		/// <summary>
		/// 初期状態
		/// </summary>
		public const int Init = 0;
		/// <summary>
		/// ワールドマップの作成
		/// </summary>
		public const int CreateWMap = 1;
		/// <summary>
		/// ワールドマップのアップロード
		/// </summary>
		public const int SendWMap = 2;
		/// <summary>
		/// 全クライアントの読み込み同期完了待ち
		/// </summary>
		public const int WaitAllPlayerWMapLoadSyncComp = 3;
		/// <summary>
		/// ステージ配置
		/// </summary>
		public const int PlaceStage = 4;
		/// <summary>
		/// ペット配置
		/// </summary>
		public const int PlacePet = 5;
		/// <summary>
		/// セットアップ完了
		/// </summary>
		public const int SetupComp = 6;
	}

	/// <summary>
	/// クライアント用モードステータスクラス
	/// </summary>
	public class CModeState
	{
		/// <summary>
		/// 初期状態
		/// </summary>
		public const int Init = 0;
		/// <summary>
		/// ワールドマップ受け取り待ち
		/// </summary>
		public const int WaitReceiveWMap = 1;
		/// <summary>
		/// ワールドマップ受け取り(ダウンロード)
		/// </summary>
		public const int ReceiveWMap = 2;
		/// <summary>
		/// ワールドマップ読み取り同期
		/// </summary>
		public const int LoadSyncWMap = 3;
		/// <summary>
		/// ステージ配置完了待ち
		/// </summary>
		public const int WaitPlaceStageComp = 4;
		/// <summary>
		/// 
		/// </summary>
		public const int PlacePet = 5;
		/// <summary>
		/// 
		/// </summary>
		public const int SetupComp = 6;
	}
	#endregion //インナークラス
}
