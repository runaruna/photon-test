﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

/// <summary>
/// マルチプレイ用オートドローン・コントローラ
/// </summary>
public class MultiAdController : AutoDroneController
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	[Header("*PhotonView Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected PhotonView m_PhotonView;
	/// <summary>
	/// [プロパティ]PhotonViewのViewID
	/// </summary>
	public int PhotonViewId 
	{
		get { return m_PhotonView.ViewID; }
	}
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected override void Awake()
	{
		base.Awake();
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected override void Start ()
	{
		base.Start();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void Update ()
	{
		base.Update();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void LateUpdate()
	{
		base.LateUpdate();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region LifeCycle
	/// <summary>
	/// LifeCycleUpdate
	/// </summary>
	protected override void lifeCycleUpdate()
	{
		base.lifeCycleUpdate();
	}
	/// <summary>
	/// ドローンのモードをランダムで返す関数
	/// </summary>
	/// <param name="_mode">1:タッチ時 2:マイク入力時</param>
	/// <returns></returns>
	protected override string getRondomADModeState()
	{
		return base.getRondomADModeState();
	}
	#endregion //LifeCycle

	// --------
	#region 移動処理関係
	/// <summary>
	/// 移動更新処理
	/// </summary>
	protected override void moveUpadte()
	{
		base.moveUpadte();
	}
	/// <summary>
	/// 移動目的地を探して移動目的地OBJをそこに移動させる
	/// </summary>
	/// <param name="callback"></param>
	protected override void searchMoveDestination(OnComp<bool> callback)
	{
		base.searchMoveDestination(callback);
	}
	/// <summary>
	/// 地面判定処理
	/// </summary>
	protected override void groundRayUpdate()
	{
		base.groundRayUpdate();
	}
	#endregion //移動処理関係

	// --------
	#region 初期化・リセット関係
	/// <summary>
	/// 初期化
	/// </summary>
	public override void init()
	{
		base.init();
	}
	#endregion //初期化・リセット関係

	// --------
	#region メンバメソッド
	/// <summary>
	/// DictのstringタイプのKeyネームをValueの確率値を元に選定して返す関数
	/// </summary>
	/// <param name="_stringKeyDict"></param>
	/// <returns></returns>
	protected override string getRandomString(Dictionary<string, int> _stringKeyDict)
	{
		return base.getRandomString(_stringKeyDict);
	}
	/// <summary>
	/// DictのintタイプのKeyネームをValueの確率値を元に選定して返す関数
	/// </summary>
	/// <param name="_intKeyDict"></param>
	/// <returns></returns>
	protected override int getRandomInt(Dictionary<int, int> _intKeyDict)
	{
		return base.getRandomInt(_intKeyDict);
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
