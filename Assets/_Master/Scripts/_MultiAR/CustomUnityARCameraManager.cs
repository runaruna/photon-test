﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.iOS;

public class CustomUnityARCameraManager : UnityARCameraManager
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	protected int m_CurrentARCameraModeState;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public int CurrentARCameraModeState 
	{
		get { return m_CurrentARCameraModeState; }
		protected set { m_CurrentARCameraModeState = value; }
	}
	/// <summary>
	/// 処理中フラグ
	/// </summary>
	protected bool m_ProcessingFlag = false;
	/// <summary>
	/// [プロパティ] 処理中フラグ
	/// </summary>
	public bool ProcessingFlag
	{
		get { return m_ProcessingFlag; }
		private set { m_ProcessingFlag = value; }
	}
	/// <summary>
	/// シリアライズ化されたWorldMapを格納するバイト型配列
	/// </summary>
	[SerializeField] protected byte[] m_SerializedWorldMap;
	/// <summary>
	/// [プロパティ]シリアライズ化されたWorldMapを格納するバイト型配列
	/// </summary>
	public byte[] SerializedWorldMap 
	{
		get { return m_SerializedWorldMap; }
		protected set { m_SerializedWorldMap = value; }
	}
	#endregion //メンバフィールド

	// --------
	#region UnityEvents
	[System.SerializableAttribute] public class CustomEventInt : UnityEvent<int> { }
	[System.SerializableAttribute] public class CustomEventBool : UnityEvent<bool> { }
	[System.SerializableAttribute] public class CustomEventByteArray : UnityEvent<byte[]> { }
	[Header("*ワールドマップSave時のイベント")]
	[SerializeField] public CustomEventByteArray m_OnSavedSerializedWorldMapEvent;
	[Header("*ワールドマップ読み込み同期完了のイベント")]
	[SerializeField] public CustomEventBool m_OnRelocalizedEvent;
	#endregion //UnityEvents

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 開始処理
	/// </summary>
	protected override void Start ()
	{
		base.Start();
		CurrentARCameraModeState = CustomARCameraModeState.NormalMode;
		UnityARSessionNativeInterface.ARFrameUpdatedEvent += onChkWorldMapStatus;
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void Update ()
	{
		base.Update();
	}
	/// <summary>
	/// 
	/// </summary>
	protected override void OnDestroy()
	{
		UnityARSessionNativeInterface.ARFrameUpdatedEvent -= onChkWorldMapStatus;
		base.OnDestroy();
	}
	#endregion //MonoBehaviourメソッド

	#region ワールドマップ関係
	/// <summary>
	/// ワールドマップとマッピングと復元の状態を確認する関数
	/// </summary>
	protected void onChkWorldMapStatus(UnityARCamera cam)
	{
		if(CurrentARCameraModeState == CustomARCameraModeState.CreateWorldMapMode)
		{
			switch (cam.worldMappingStatus)
			{
				case ARWorldMappingStatus.ARWorldMappingStatusMapped:
					CurrentARCameraModeState = CustomARCameraModeState.NormalMode;
					saveSelializedWorldMap();
					break;
				case ARWorldMappingStatus.ARWorldMappingStatusExtending:
				case ARWorldMappingStatus.ARWorldMappingStatusLimited:
				case ARWorldMappingStatus.ARWorldMappingStatusNotAvailable:
				default:
					break;
			}
		}
		else if(CurrentARCameraModeState == CustomARCameraModeState.RelocalizeMode)
		{
			if (cam.trackingState == ARTrackingState.ARTrackingStateNormal &&
				cam.trackingReason == ARTrackingStateReason.ARTrackingStateReasonNone)
			{
				CurrentARCameraModeState = CustomARCameraModeState.NormalMode;
				m_OnRelocalizedEvent.Invoke(true);
			}
		}
	}
	/// <summary>
	/// ワールドマップ作成モードに変更する関数
	/// </summary>
	public void chgCreateWorldMapMode()
	{
		if(ProcessingFlag)
		{
			return;
		}
		else
		{
			ProcessingFlag = true;
		}

		var conf = sessionConfiguration;
		UnityARSessionRunOption opt = UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors | UnityARSessionRunOption.ARSessionRunOptionResetTracking;
		m_session.RunWithConfigAndOptions(conf, opt);
		CurrentARCameraModeState = CustomARCameraModeState.CreateWorldMapMode;
		ProcessingFlag = false;
	}
	/// <summary>
	/// ARWorldMapをバイト型配列に変換して変数に格納する関数
	/// </summary>
	protected void saveSelializedWorldMap()
	{
		if(ProcessingFlag)
		{
			return;
		}
		else
		{
			ProcessingFlag = true;
		}

		m_session.GetCurrentWorldMapAsync((ARWorldMap arwm)=>
		{
			if(arwm != null)
			{
				SerializedWorldMap = arwm.SerializeToByteArray();
				m_OnSavedSerializedWorldMapEvent.Invoke(SerializedWorldMap);
				ProcessingFlag = false;
				Debug.Log("mob- carcm- worldmap saved...");
			}
			else
			{
				ProcessingFlag = false;
			}
		});
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="_serializedWorldMap"></param>
	public void loadSelializedWorldMap(byte[] _serializedWorldMap)
	{
		if (ProcessingFlag)
		{
			return;
		}
		else
		{
			ProcessingFlag = true;
		}

		ARWorldMap arwm = ARWorldMap.SerializeFromByteArray(_serializedWorldMap);

		if(arwm != null)
		{
			SerializedWorldMap = _serializedWorldMap;
			UnityARSessionNativeInterface.ARSessionShouldAttemptRelocalization = true;
			var conf = sessionConfiguration;
			conf.worldMap = arwm;
			UnityARSessionRunOption opt = UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors | UnityARSessionRunOption.ARSessionRunOptionResetTracking;
			m_session.RunWithConfigAndOptions(conf, opt);
			CurrentARCameraModeState = CustomARCameraModeState.RelocalizeMode;
			ProcessingFlag = false;
		}
		else
		{
			ProcessingFlag = false;
		}
	}
	#endregion //ワールドマップ関係

	// --------
	#region メンバメソッド
	/// <summary>
	/// デバックプレーン消す(使用後、再起動するまでデバックプレーンは生成されない)
	/// </summary>
	public void reset()
	{

		ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration();
		config.planeDetection = UnityARPlaneDetection.None;
		config.alignment = startAlignment;
		config.getPointCloudData = getPointCloud;
		config.enableLightEstimation = enableLightEstimation;
		m_session.RunWithConfigAndOptions(config, UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors);

		Debug.Log("カメラマネージャーをリセット");
	}

	/// <summary>
	/// カメラマネージャーを再起動
	/// </summary>
	public void reboot()
	{
		StartCoroutine(_reboot());
	}
	IEnumerator _reboot()
	{

		ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration();
		config.planeDetection = UnityARPlaneDetection.None;
		config.alignment = startAlignment;
		config.getPointCloudData = getPointCloud;
		config.enableLightEstimation = enableLightEstimation;
		m_session.RunWithConfigAndOptions(config, UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors);

		yield return new WaitForSeconds(3.5f);

		m_session.RunWithConfigAndOptions(sessionConfiguration, UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors);

		Debug.Log("カメラマネージャーを再起動！");
	}

	protected override void FirstFrameUpdate(UnityARCamera cam)
	{
		base.FirstFrameUpdate(cam);
	}

	public override void SetCamera(Camera newCamera)
	{
		base.SetCamera(newCamera);
	}

	protected override void SetupNewCamera(Camera newCamera)
	{
		base.SetupNewCamera(newCamera);
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	/// <summary>
	/// 
	/// </summary>
	/// <value></value>
	public class CustomARCameraModeState
	{
		/// <summary>
		/// 通常モード
		/// </summary>
		public const int NormalMode = 0;
		/// <summary>
		/// ワールドマップ作成モード
		/// </summary>
		public const int CreateWorldMapMode = 1;
		/// <summary>
		/// ワールドマップ読み込み同期モード
		/// </summary>
		public const int RelocalizeMode = 2;

		// --------
		#region コンストラクタ
		public CustomARCameraModeState()
		{
		}
		#endregion //コンストラクタ
	}
	#endregion //インナークラス
}
