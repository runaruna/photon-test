﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class CustomPointCloudParticleExample : PointCloudParticleExample
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	private bool isUpdate = true;   //鬼追加
	#endregion //メンバフィールド

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 開始処理
	/// </summary>
	protected override void Start ()
	{
		base.Start();
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected override void Update ()
	{
		base.Update();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	/// <param name="camera"></param>
	public override void ARFrameUpdated(UnityARCamera camera)
	{
		base.ARFrameUpdated(camera);
	}
	//--------------------------------------------------------------------
	//鬼追加
	public void ParticleOff()
	{
		ParticleSystem.Particle[] particles = new ParticleSystem.Particle[1];
		particles[0].startSize = 0.0f;
		currentPS.SetParticles(particles, 1);

		isUpdate = false;
	}

	public void ParticleOn()
	{
		isUpdate = true;
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
