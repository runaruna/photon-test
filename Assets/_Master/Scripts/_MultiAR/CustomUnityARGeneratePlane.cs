﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class CustomUnityARGeneratePlane : UnityARGeneratePlane
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// Custom AR Camera マネージャ
	/// </summary>
	[SerializeField] protected CustomUnityARCameraManager m_ARCameraManager;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 開始処理
	/// </summary>
	protected override void Start ()
	{
		base.Start();
	}
	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	protected override void OnDestroy()
	{
		base.OnDestroy();
	}
	/// <summary>
	/// Raises the GU event.
	/// </summary>
	protected override void OnGUI()
	{
		base.OnGUI();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	/// <summary>
	/// Gets the AR plane count.
	/// </summary>
	/// <returns>The AR plane count.</returns>
	public int getARPlaneCount()
	{
		int cnt = 0;
		LinkedList<ARPlaneAnchorGameObject> arpags = unityARAnchorManager.GetCurrentPlaneAnchors();

		cnt = arpags.Count;

		// Debug.Log ("MOB^平面の数："+cnt);

		return cnt;
	}
	/// <summary>
	/// プレーンを削除してプレーン生成を止める(プレーンの代わりに空のGameObjectを生成する)
	/// </summary>
	public void stopGeneratePlane()
	{
		Debug.Log("mob- cuargp- stopGeneratePlane");
		unityARAnchorManager.Destroy();
	}
	/// <summary>
	/// プレーンの生成を再開する
	/// (アンカー情報も削除する必用があるためARCameraManagerもrebootさせる)
	/// </summary>
	public void reStartGeneratePlane()
	{
		Debug.Log("mob- cuargp- reStartGeneratePlane");
		m_ARCameraManager.reboot(); //アンカー情報が残っているとプレーンが生成されないのでARCameraManagerもrebootさせる
		unityARAnchorManager = new UnityARAnchorManager();
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
