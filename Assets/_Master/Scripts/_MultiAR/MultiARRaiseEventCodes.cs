﻿using System.Collections;
using System.Collections.Generic;

public class MultiARRaiseEventCodes:BaseRaiseEventCodes
{
	// --------
	#region メンバフィールド
	/// <summary>
	/// マスタークライアントのモード変更
	/// </summary>
	public const byte ChgMCModeState = 1;
	/// <summary>
	/// ワールドマップをアップロードした
	/// </summary>
	public const byte WorldMapUploaded = 2;
	/// <summary>
	/// ワールドマップの読み込みと同期が完了した
	/// </summary>
	public const byte WorldMapLoadSyncCompleted = 3;
	/// <summary>
	/// ステージOJBのViewIdを受信した
	/// </summary>
	public const byte ReceiveStageViewId = 4;
	/// <summary>
	/// オートドローンOJBのViewIdを受信した
	/// </summary>
	public const byte ReceiveAdViewId = 5;
	#endregion //メンバフィールド


	// --------
	#region コンストラクタ
	/// <summary>
	/// 
	/// </summary>
	public MultiARRaiseEventCodes():base()
	{
	}
	#endregion //コンストラクタ
}
