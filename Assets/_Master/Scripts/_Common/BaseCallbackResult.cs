﻿using System.Collections;
using System.Collections.Generic;

/// <summary>
/// コールバックリザルトクラス
/// </summary>
public class BaseCallbackResult {

	// --------
	#region メンバフィールド
	/// <summary>
	/// 結果フラグ
	/// </summary>
	public bool result;
	/// <summary>
	/// int型の結果データ
	/// </summary>
	public int intData;
	/// <summary>
	/// float型の結果データ
	/// </summary>
	public float floatData;
	/// <summary>
	/// string型の結果データ
	/// </summary>
	public string stringData;
	/// <summary>
	/// エラーコード
	/// </summary>
	public string errorCode;
	/// <summary>
	/// エラーメッセージ
	/// </summary>
	public string errorMessage;
	#endregion //メンバフィールド

	// --------
	#region コンストラクタ
	public BaseCallbackResult(){

	}
	#endregion //コンストラクタ
}
