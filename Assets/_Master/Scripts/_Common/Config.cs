﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour {

	// シングルトン宣言 ------------------------------------
	private static Config mInstance;
	// コンストラクタ
	private Config() {

	}
	// インスタンスを生成する
	public static Config Instance {
		get {
			if (mInstance == null) {
				GameObject gObj = new GameObject("Config");
				mInstance = gObj.AddComponent<Config>();
			}
			return mInstance;
		}
	}
	// --------------------------------------------------

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	///
	/// </summary>
	#endregion

	// --------
	#region メンバフィールド
	/// <summary>
	/// AESで暗号・復号に使うパスワード
	/// </summary>
	public const string AesPassword = "avexcsrs";

	/// <summary>
	/// Scene name.
	/// </summary>
	public class SceneName
	{
		public const string SPLASH = "Splash";    //スプラッシュ
		public const string CAUTION = "Caution";   //注意事項
		public const string AGREEMENT = "Agreement"; //利用規約
		public const string DOWNLOAD = "Download";  //アセットバンドルDL
		public const string DATA_CHECK = "DataCheck"; //データチェック
		public const string PET_CREATE = "PetCreate"; //キャラ作成
		public const string PET_PROFILE = "PetProfile"; //キャラプロフィール
		public const string AR = "AR"; //ARシーン
		public const string SHOP = "Shop"; //ショップシーン
		#if !COIN_SHOP_ENT
		public const string COIN_SHOP = "CoinShop"; //コインショップシーン
		#else
		public const string COIN_SHOP = "CoinShopEnterprise"; //コインショップシーン(エンタープライズ版用)
		#endif
	}

	/// <summary>
	/// Player prefs key.
	/// </summary>
	public class PlayerPrefsKey
	{
		public const string AGREEMENT = "Agreement"; //利用規約
		public const string HOWTO = "HOWTO"; //初回起動時使い方ポップアップ表示フラグ
		public const string IS_FIRST_DOWNLOAD = "is_first_download";
		public const string IS_PET_CREATED = "IS_PET_CREATED"; //ペットを作成したことがあるかフラグ
	}

	/// <summary>
	/// the tag names.
	/// </summary>
	public class TagNames
	{
		public const string ARKIT_PLANE = "ARKitPlane";
		public const string ARKIT_PLANE_PARENT = "ARKitPlaneParent";
		public const string FIELD_MARKER = "FieldMarker";
		public const string LASER_MARKER = "LaserMarker";
		public const string MOVE_DESTINATION = "MoveDestination"; //移動目的地
		public const string NO_ENTRY = "NoEntry"; //進入禁止
		public const string GIFT = "Gift"; //
		public const string BALL = "Ball"; //Ballタイプのアイテムに使う
		public const string CUSHION = "Cushion"; //Cushionタイプのアイテムに使う
		public const string RAT_TOY = "RatToy"; //RatToyタイプのアイテムに使う
		public const string POT = "Pot"; //Potタイプのアイテムに使う
		public const string CAT_TOWER = "CatTower"; //CatTowerタイプのアイテムに使う
		public const string SCRATCH_BOARD = "ScratchBoard"; //ScratchBoardタイプのアイテムに使う
		public const string CARDBOARD = "Cardboard"; //Cardboardタイプのアイテムに使う
		public const string FEED_DISH = "FeedDish"; //FeedDishタイプのアイテムに使う
		public const string WATER_TRAY = "WaterTray"; //WaterTrayタイプのアイテムに使う
		public const string BOX = "Box"; //Boxタイプのアイテムに使う
		public const string FOOD = "Food"; //Foodタイプのアイテムに使う(廃止予定)
		public const string CHURU = "Churu"; //Churuタイプのアイテムに使う
	}

	/// <summary>
	/// アイテムのカテゴリ
	/// (フィルタリング・ソート用)
	/// </summary>
	public class ItemCategories
	{
		/// <summary>
		/// おもちゃ
		/// </summary>
		public const int TOY = 0;
		/// <summary>
		/// グッズ
		/// </summary>
		public const int GOODS = 1;
		/// <summary>
		/// ご飯皿
		/// </summary>
		public const int FEED_DISH = 2;
		/// <summary>
		/// 水飲み皿
		/// </summary>
		public const int WATER_TRAY = 3;
		/// <summary>
		/// 食べ物
		/// </summary>
		public const int FOOD = 4;
	}
	/// <summary>
	/// アイテムのプライマリタイプ
	/// </summary>
	public class ItemPrimaryType
	{
		/// <summary>
		/// 配置型
		/// </summary>
		public const int PLACEMENT = 0;
		/// <summary>
		/// 手持ち型
		/// </summary>
		public const int HANDY = 1;
	}
	/// <summary>
	/// アイテムのセカンダリタイプ
	/// </summary>
	public class ItemSecondaryType
	{
		/// <summary>
		/// BOX(ボックス)タイプ
		/// </summary>
		public const string BOX = "Box";
		/// <summary>
		/// BALL(ボール)タイプ
		/// </summary>
		public const string BALL = "Ball";
		/// <summary>
		/// CUSHION(クッション)タイプ
		/// </summary>
		public const string CUSHION = "Cushion";
		/// <summary>
		/// RAT_TOY(ネズミのおもちゃ)タイプ
		/// </summary>
		public const string RAT_TOY = "RatToy";
		/// <summary>
		/// POT(土鍋)タイプ
		/// </summary>
		public const string POT = "Pot";
		/// <summary>
		/// CAT_TOWER(キャットタワー)タイプ
		/// </summary>
		public const string CAT_TOWER = "CatTower";
		/// <summary>
		/// SCRATCH_BOARD(爪研ぎボード)タイプ
		/// </summary>
		public const string SCRATCH_BOARD = "ScratchBoard";
		/// <summary>
		/// CARDBOARD(段ボール)タイプ
		/// </summary>
		public const string CARDBOARD = "Cardboard";
		/// <summary>
		/// FEED_DISH(餌皿)タイプ
		/// </summary>
		public const string FEED_DISH = "FeedDish";
		/// <summary>
		/// WATER_TRAY(水飲み皿)タイプ
		/// </summary>
		public const string WATER_TRAY = "WaterTray";
		/// <summary>
		/// FOOD(餌)タイプ
		/// </summary>
		public const string FOOD = "Food";
		/// <summary>
		/// CHURU(チュール)タイプ
		/// </summary>
		public const string CHURU = "Churu";
	}
	/// <summary>
	/// アイテムの製品タイプ
	/// </summary>
	public class ItemProductTypes
	{
		/// <summary>
		/// 消費型
		/// </summary>
		public const int CONSUMABLE = 0;
		/// <summary>
		/// 非消費型
		/// </summary>
		public const int NON_CONSUMABLE = 1;
	}

	/// <summary>
	/// ペットデータのエラーコード
	/// (ERR-001)
	/// </summary>
	public class PetDataErrorCodes
	{
		/// <summary>
		/// 正常
		/// (0)
		/// </summary>
		public const string OK = "0";
		/// <summary>
		/// ペットデータのファイルが見つからなかった
		/// (ERR-001-000-000)
		/// </summary>
		public const string FILE_NOT_FOUND = "ERR-001-000-000";
		/// <summary>
		/// ペットデータのファイルの読み込みに失敗した
		/// (ERR-001-000-001)
		/// </summary>
		public const string FILE_READ_ERROR = "ERR-001-000-001";
		/// <summary>
		/// ペットデータのファイルのバージョンが現行バージョンと異なる
		/// (ERR-001-001-000)
		/// </summary>
		public const string DIFFERENT_VERSION = "ERR-001-001-000";
	}

	/// <summary>
	/// アイテムデータリストのエラーコード
	/// (ERR-002)
	/// </summary>
	public class ItemDataListErrorCodes
	{
		/// <summary>
		/// 正常
		/// (0)
		/// </summary>
		public const string OK = "0";
		/// <summary>
		/// スアイテムデータディクトのファイルが見つからなかった
		/// (ERR-002-000-000)
		/// </summary>
		public const string FILE_NOT_FOUND = "ERR-002-000-000";
		/// <summary>
		/// アイテムデータディクトのファイルの読み込みに失敗した
		/// (ERR-002-000-001)
		/// </summary>
		public const string FILE_READ_ERROR = "ERR-002-000-001";
		/// <summary>
		/// アイテムデータディクトのファイルのバージョンが現行バージョンと異なる
		/// (ERR-002-001-000)
		/// </summary>
		public const string DIFFERENT_VERSION = "ERR-002-001-000";
	}

	/// <summary>
	/// ウォレットデータのエラーコード
	/// (ERR-003)
	/// </summary>
	public class WalletDataErrorCodes
	{
		/// <summary>
		/// 正常
		/// (0)
		/// </summary>
		public const string OK = "0";
		/// <summary>
		/// ウォレットデータのファイルが見つからなかった
		/// (ERR-003-000-000)
		/// </summary>
		public const string FILE_NOT_FOUND = "ERR-003-000-000";
		/// <summary>
		/// ウォレットデータのファイルの読み込みに失敗した
		/// (ERR-003-000-001)
		/// </summary>
		public const string FILE_READ_ERROR = "ERR-003-000-001";
		/// <summary>
		/// ウォレットデータのファイルのバージョンが現行バージョンと異なる
		/// (ERR-003-001-000)
		/// </summary>
		public const string DIFFERENT_VERSION = "ERR-003-001-000";
	}

	/// <summary>
	/// Pet data name.
	/// </summary>
	public class PetDataFileName {
		public const string D1 = "PetData_1";
		public const string D2 = "PetData_2";
		public const string D3 = "PetData_3";

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public PetDataFileName()
		{

		}
		/// <summary>
		/// ペットデータjsonファイルの名前を配列で返す
		/// </summary>
		/// <returns></returns>
		public static string[] getPetDataFileNames()
		{
			string[] result = {
				D1,
				D2,
				D3
			};
			return result;
		}
	}

	/// <summary>
	/// ペットデータファイルの拡張子
	/// </summary>
	public const string PetDataFileExt = ".dat";

	/// <summary>
	/// ペットデータのバージョン
	/// </summary>
	public const string PetDataVersion = "0.3.1"; //ペットデータクラスを変更した場合はここも変更すること！

	/// <summary>
	/// ウォレットデータのバージョン
	/// </summary>
	public const string WalletDataVersion = "1.0.0"; //変更することは二度とない
	/// <summary>
	/// ウォレットデータファイルの拡張子
	/// </summary>
	public const string WalletDataFileExt = ".dat";
	/// <summary>
	/// ウォレットデータファイルの名前
	/// </summary>
	public const string WalletDataFileName = "WalletData";

	/// <summary>
	/// 初期所持アイテムのIDリスト
	/// </summary>
	public static readonly string[] DefaultItemIdList = {
		"item.boxx001",
		"item.boxx002",
		"item.boxx003",
		"item.boxx004"
	};
	/// <summary>
	/// 初期所持アイテムのカテゴリリスト
	/// </summary>
	public static readonly int[] DefautlItemCategoryList = {
		Config.ItemCategories.GOODS,
		Config.ItemCategories.GOODS,
		Config.ItemCategories.GOODS,
		Config.ItemCategories.GOODS
	};
	/// <summary>
	/// 初期所持アイテムのプライマリタイプリスト
	/// </summary>
	public static readonly int[] DefautlItemPrimaryTypeList = {
		Config.ItemPrimaryType.PLACEMENT,
		Config.ItemPrimaryType.PLACEMENT,
		Config.ItemPrimaryType.PLACEMENT,
		Config.ItemPrimaryType.PLACEMENT,
	};

	/// <summary>
	/// 初期所持アイテムのセカンダリタイプリスト
	/// </summary>
	public static readonly string[] DefaultItemSecondaryTypeList = {
		Config.ItemSecondaryType.BOX,
		Config.ItemSecondaryType.BOX,
		Config.ItemSecondaryType.BOX,
		Config.ItemSecondaryType.BOX
	};

	/// <summary>
	/// 初期所持アイテムの製品タイプリスト
	/// </summary>
	public static readonly int[] DefautlItemProductTypesList = {
		Config.ItemProductTypes.NON_CONSUMABLE,
		Config.ItemProductTypes.NON_CONSUMABLE,
		Config.ItemProductTypes.NON_CONSUMABLE,
		Config.ItemProductTypes.NON_CONSUMABLE,
	};
	/// <summary>
	/// 初期所持アイテムの量のリスト
	/// </summary>
	public static readonly int[] DefaultItemAmountList = {
		-9999,
		-9999,
		-9999,
		-9999
	};

	/// <summary>
	/// アイテムデータリストのバージョン
	/// </summary>
	public const string ItemDataListVersion = "1.0.0"; //変更することはない(ItemDataのVersionと合わせる)
	/// <summary>
	/// アイテムデータリストのファイル拡張子
	/// </summary>
	public const string ItemDataListFileExt = ".dat";
	/// <summary>
	/// アイテムデータリストのファイル名称
	/// </summary>
	public const string ItemDataListFileName = "ItemDataList";
	/// <summary>
	/// アイテムデータのバージョン
	/// </summary>
	public const string ItemDataVersion = "1.0.0"; //変更することはない(ItemDataListのVersionと合わせる)

	/// <summary>
	/// DateTimeの文字列フォーマット
	/// </summary>
	public const string DateTimeFormat = "yyyy.MM.dd.HH.mm.ss";

	/// <summary>
	/// DateTimeの文字列を分割する際に用いる文字
	/// </summary>
	public const char DateTimeSplitCharacter = '.';

	/// <summary>
	/// お問い合わせ(宛先)
	/// </summary>
	public const string ContactMailTo = "ieneko@avex.jp";

	/// <summary>
	/// お問い合わせ(件名)
	/// </summary>
	public const string ContactSubject = "お問い合わせの件名が入ります";

	/// <summary>
	/// お問い合わせ(本文)
	/// </summary>
	public const string ContactBody = "お問い合わせの本文が配慮";

	/// <summary>
	/// 広告非表示フラグ格納フィールド
	/// </summary>
	public static bool adHideFlag = false;
	#endregion

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	void Awake() {

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	void Start () {

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	void Update () {

	}

	/// <summary>
	/// 更新処理
	/// </summary>
	void LateUpdate(){

	}
	#endregion

	// --------
	#region メンバメソッド
	#endregion
}
