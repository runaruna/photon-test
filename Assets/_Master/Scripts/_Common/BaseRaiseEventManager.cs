﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// ベースRaiseEventManager
/// </summary>
public class BaseRaiseEventManager : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 初期化完了フラグ
	/// </summary>
	protected bool m_InitCompFlag = false;
	/// <summary>
	/// [プロパティ] 初期化完了フラグ
	/// </summary>
	public bool InitCompFlag
	{
		get { return m_InitCompFlag; }
		protected set { m_InitCompFlag = value; }
	}
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	/// <summary>
	/// 
	/// </summary>
	protected virtual void OnEnable()
	{
		Debug.Log("mob- OnEnable");
		PhotonNetwork.NetworkingClient.EventReceived += OnEvent; //RaiseEventの登録
		InitCompFlag = true;
	}
	/// <summary>
	/// 
	/// </summary>
	protected virtual void OnDisable()
	{
		Debug.Log("mob- OnDisable");
		PhotonNetwork.NetworkingClient.EventReceived -= OnEvent; //RaiseEventの破棄
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region RaiseEvent
	/// <summary>
	/// RaiseEvent
	/// </summary>
	protected virtual void OnEvent(EventData _photonEvent)
	{
		Debug.Log("mob- mgs- oe- _photonEvent.Code : [" + _photonEvent.Code + "] _photonEvent.Sender : [" + _photonEvent.Sender + "]");
	}
	/// <summary>
	/// 
	/// </summary>
	public virtual void sendRaiseEventMessageTest()
	{
		string message = "send RaseEvent...";
		var raiseEventOptions = new RaiseEventOptions()
		{
			Receivers = ReceiverGroup.All,
			CachingOption = EventCaching.DoNotCache
		};
		PhotonNetwork.RaiseEvent(BaseRaiseEventCodes.SendTest, message, raiseEventOptions, SendOptions.SendReliable);
	}
	#endregion //RaiseEvent

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
