﻿using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Photon.Pun;
using Photon.Realtime;


/// <summary>
/// PUN用ベースシーンディレクタークラス
/// </summary>
public class BasePunSceneDirector : MonoBehaviourPunCallbacks
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	[Header("*Initial Display Fade Settings")]
	/// <summary>
	/// 初期表示時のフェード処理の時間
	/// </summary>
	[SerializeField] protected float initialDisplayFadeDurationTime;
	/// <summary>
	/// 初期表示時のフェードの色
	/// </summary>
	[SerializeField] protected Color initialDisplayFadeColor;

	[Header("*Scene Translate Settings")]
	/// <summary>
	/// フェード開始までの遅延時間
	/// </summary>
	[SerializeField] protected float fadeStartDelayTime;
	/// <summary>
	/// フェード時間
	/// </summary>
	public float fadeDurationTime;
	/// <summary>
	/// フェード後の待ち時間
	/// </summary>
	public float waitTimeAfterFade;
	/// <summary>
	/// フェードカラー
	/// </summary>
	public Color fadeColor;
	/// <summary>
	/// イベントシステム格納フィールド
	/// </summary>
	[SerializeField] protected EventSystem m_EventSystem;

	[Header("*Screen AutoRotaion Settings")]
	/// <summary>
	/// 画面自動回転を自動的に有効化するフラグ
	/// </summary>
	[SerializeField] protected bool m_AutoSetupToScreenAutoRotation = false;
	/// <summary>
	/// The allowed autorotate to portrait.
	/// </summary>
	[SerializeField] protected bool m_AllowedAutorotateToPortrait = true;
	/// <summary>
	/// The allowed autorotate to portrait upside down.
	/// </summary>
	[SerializeField] protected bool m_AllowedAutorotateToPortraitUpsideDown = false;
	/// <summary>
	/// The allowed autorotate to landscape left.
	/// </summary>
	[SerializeField] protected bool m_AllowedAutorotateToLandscapeLeft = true;
	/// <summary>
	/// The allowed autorotate to landscape right.
	/// </summary>
	[SerializeField] protected bool m_AllowedAutorotateToLandscapeRight = true;

	[Header("*Touch Settings")]
	/// <summary>
	/// マルチタッチ有効化フラグ
	/// </summary>
	[SerializeField] protected bool m_multiTouchEnabled = true;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// The scene translate flag.
	/// </summary>
	protected bool sceneTranslatingFlag = false;
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		//マルチタッチ有効・無効の設定
		Input.multiTouchEnabled = m_multiTouchEnabled;
		if (m_AutoSetupToScreenAutoRotation)
		{
			setupToScreenAutoRotaion();
		}

		PhotonNetwork.AutomaticallySyncScene = true; //Room内のクライアントがすべて、（PhotonNetwork.LoadLevelを使っている場合）Master Clientと同じレベルをロードするべきかを決めます。
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		ScreenFadeManager.Instance.FadeIn(initialDisplayFadeDurationTime, initialDisplayFadeColor, () =>{});
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド

	#region PUN コールバック
	/// <summary>
	/// PhotonNetwork.autoJoinLobby が false の状態で、
	/// マスターサーバーへ接続および認証が完了した際に呼び出されます。
	/// </summary>
	public override void OnConnectedToMaster()
	{
		Debug.Log("mob- OnConnectedToMaster");
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="cause"></param>
	public override void OnDisconnected(DisconnectCause cause)
	{
		Debug.Log("mob- OnDisconnected : "+cause);
	}
	/// <summary>
	/// Master Serverのロビーに入るときに呼び出されます
	/// </summary>
	public override void OnJoinedLobby()
	{
		Debug.Log("mob- OnJoinedLobby");
	}
	/// <summary>
	/// ロビーを退出した際に呼び出されます。
	/// </summary>
	public override void OnLeftLobby()
	{
		Debug.Log("mob- OnLeftLobby");
	}
	/// <summary>
	/// Master Serverのロビー（InLobby）にいる間に、ルームリストが更新されるたびに呼び出されます。
	/// </summary>
	/// <param name="roomList"></param>
	public override void OnRoomListUpdate(List<RoomInfo> roomList)
	{
		foreach (RoomInfo v in roomList)
		{
			Debug.Log("mob- OnRoomListUpdate : " + v.Name);
		}
	}
	/// <summary>
	/// ルームの作成に成功したときに呼び出される
	/// </summary>
	public override void OnCreatedRoom()
	{
		Debug.Log("mob- OnCreatedRoom");
	}
	/// <summary>
	/// ルームの作成に失敗したときに呼び出される
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnCreateRoomFailed(short returnCode, string message)
	{
		Debug.Log("mob- OnCreateRoomFaild- returnCode : ["+returnCode+"] message : ["+message+"]");
	}
	/// <summary>
	/// ルームの入室に失敗したときに呼び出される
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRoomFailed(short returnCode, string message)
	{
		Debug.Log("mob- OnJoinRoomFailed- returnCode : [" + returnCode + "] message : [" + message + "]");
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		Debug.Log("mob- OnJoinRandomFailed- returnCode : [" + returnCode + "] message : [" + message + "]");
	}
	/// <summary>
	/// 部屋に入室した際に呼ばれる
	/// </summary>
	public override void OnJoinedRoom()
	{
		Debug.Log("mob- OnJoinRoom");
	}
	/// <summary>
	/// 部屋から退出した際に呼ばれる
	/// </summary>
	public override void OnLeftRoom()
	{
		Debug.Log("mob- OnLeftRoom");
	}
	/// <summary>
	/// リモートプレイヤーが部屋に入室した際に呼ばれる
	/// (このプレーヤーは既にプレーヤーリストに追加されています)
	/// </summary>
	/// <param name="newPlayer"></param>
	public override void OnPlayerEnteredRoom(Player newPlayer)
	{
		Debug.Log("mob- OnPlayerEnteredRoom");
		Debug.Log("mob- oper- newPlayer.ActorNumber : " + newPlayer.ActorNumber);
		Debug.Log("mob- oper- newPlayer.NickName : " + newPlayer.NickName);
	}
	/// <summary>
	/// リモートプレイヤーが部屋を離れるか、非アクティブになったときに呼び出されます。
	/// </summary>
	/// <param name="otherPlayer"></param>
	public override void OnPlayerLeftRoom(Player otherPlayer)
	{
		Debug.Log("mob- OnPlayerLeftRoom");
		Debug.Log("mob- oplr- otherPlayer.ActorNumber : "+otherPlayer.ActorNumber);
		Debug.Log("mob- oplr- otherPlayer.NickName : " + otherPlayer.NickName);
	}
	/// <summary>
	/// 現在のMasterClientが終了したときに新しいMasterClientに切り替えた後に呼び出されます。
	/// </summary>
	/// <param name="newMasterClient"></param>
	public override void OnMasterClientSwitched(Player newMasterClient)
	{
		Debug.Log("mob- OnMasterClientSwitched");
		Debug.Log("mob- omcs- newMasterClient.ActorNumber :"+newMasterClient.ActorNumber);
		Debug.Log("mob- omcs- newMasterClient.NickName :" + newMasterClient.NickName);
		if(PhotonNetwork.IsMasterClient)
		{
			Debug.Log("mob- omcs- You are the master client!");
		}
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="target"></param>
	/// <param name="changedProps"></param>
	public override void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
	{
		Debug.Log("mob- OnPlayerPropertiesUpdate");
		Debug.Log("mob- oppu- target.ActorNumber : "+target.ActorNumber);
		Debug.Log("mob- oppu- target.NickName : "+target.NickName);
		Debug.Log("====================");
		Debug.Log("=== changedProps ===");
		Debug.Log("====================");
		foreach (var v in changedProps)
		{
			Debug.Log(v);
		}
		Debug.Log("====================");
	}
	#endregion //PUN コールバック

	// --------
	#region 画面回転関係
	/// <summary>
	/// 画面自動回転セットアップ関数
	/// </summary>
	public virtual void setupToScreenAutoRotaion()
	{
		Screen.autorotateToLandscapeLeft = m_AllowedAutorotateToLandscapeLeft;
		Screen.autorotateToLandscapeRight = m_AllowedAutorotateToLandscapeRight;
		Screen.autorotateToPortrait = m_AllowedAutorotateToPortrait;
		Screen.autorotateToPortraitUpsideDown = m_AllowedAutorotateToPortraitUpsideDown;
		Screen.orientation = ScreenOrientation.AutoRotation;
	}
	#endregion //画面回転関係

	// --------
	#region シーン遷移関係
	/// <summary>
	/// Translates the scene.
	/// </summary>
	/// <param name="_sceneName">Scene name.</param>
	public virtual void translateScene(string _sceneName)
	{
		if (!sceneTranslatingFlag)
		{
			sceneTranslatingFlag = true;
			if (m_EventSystem != null)
			{
				m_EventSystem.enabled = false;
			}
			StartCoroutine(waitTimer(fadeStartDelayTime, () =>
			{
				ScreenFadeManager.Instance.FadeOut(fadeDurationTime, fadeColor, () =>
				{
					StartCoroutine(_translateScene(_sceneName, waitTimeAfterFade));
				});
			}));
		}
	}
	/// <summary>
	/// Translates the scene.
	/// </summary>
	/// <returns>The scene.</returns>
	/// <param name="_sceneName">Scene name.</param>
	/// <param name="_waitTime">Wait time.</param>
	protected IEnumerator _translateScene(string _sceneName, float _waitTime = 0)
	{
		yield return new WaitForSeconds(_waitTime);
		SceneManager.LoadScene(_sceneName);
		if (m_EventSystem != null)
		{
			m_EventSystem.enabled = true;
		}
		sceneTranslatingFlag = false;
	}

	/// <summary>
	/// Translates the scene landscape.
	/// </summary>
	/// <param name="_sceneName">Scene name.</param>
	public virtual void translateSceneLandscape(string _sceneName)
	{

		if (!sceneTranslatingFlag)
		{

			sceneTranslatingFlag = true;

			if (m_EventSystem != null)
			{
				m_EventSystem.enabled = false;
			}

			StartCoroutine(waitTimer(fadeStartDelayTime, () =>
			{
				ScreenFadeManager.Instance.FadeOut(fadeDurationTime, fadeColor, () =>
				{
					StartCoroutine(_translateSceneLandscape(_sceneName, waitTimeAfterFade));
				});
			}));
		}

	}
	/// <summary>
	/// [コルーチン]Translates the scene landscape.
	/// </summary>
	/// <param name="_sceneName"></param>
	/// <param name="_waitTime"></param>
	protected IEnumerator _translateSceneLandscape(string _sceneName, float _waitTime = 0)
	{
		yield return new WaitForSeconds(_waitTime);
		//強制的にLandscapeに変更してからシーン遷移
		Screen.orientation = ScreenOrientation.Landscape;
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.autorotateToLandscapeLeft = true;
		Screen.autorotateToLandscapeRight = true;

		SceneManager.LoadScene(_sceneName);
		if (m_EventSystem != null)
		{
			m_EventSystem.enabled = true;
		}
		sceneTranslatingFlag = false;
	}
	/// <summary>
	/// Translates the scene Portrait.
	/// </summary>
	/// <param name="_sceneName">Scene name.</param>
	public virtual void translateScenePortrait(string _sceneName)
	{
		if (!sceneTranslatingFlag)
		{
			sceneTranslatingFlag = true;
			if (m_EventSystem != null)
			{
				m_EventSystem.enabled = false;
			}
			StartCoroutine(waitTimer(fadeStartDelayTime, () =>
			{
				ScreenFadeManager.Instance.FadeOut(fadeDurationTime, fadeColor, () =>
				{
					StartCoroutine(_translateScenePortrait(_sceneName, waitTimeAfterFade));
				});
			}));
		}
	}
	/// <summary>
	/// [コルーチン]Translates the scene Portrait.
	/// </summary>
	/// <param name="_sceneName"></param>
	/// <param name="_waitTime"></param>
	protected IEnumerator _translateScenePortrait(string _sceneName, float _waitTime = 0)
	{
		yield return new WaitForSeconds(_waitTime);

		//強制的にPortraitに変更してからシーン遷移
		Screen.orientation = ScreenOrientation.Portrait;
		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.autorotateToLandscapeLeft = false;
		Screen.autorotateToLandscapeRight = false;

		SceneManager.LoadScene(_sceneName);
		if (m_EventSystem != null)
		{
			m_EventSystem.enabled = true;
		}
		sceneTranslatingFlag = false;
	}
	#endregion //シーン遷移関係

	// --------
	#region メンバメソッド
	/// <summary>
	/// Waits the timer.
	/// </summary>
	/// <returns>The timer.</returns>
	/// <param name="waitTime">Wait time.</param>
	/// <param name="t_cb">T cb.</param>
	protected IEnumerator waitTimer(float waitTime, OnComplete callback)
	{
		yield return new WaitForSeconds(waitTime);
		callback();
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
