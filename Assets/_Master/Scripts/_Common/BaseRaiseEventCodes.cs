﻿using System.Collections;
using System.Collections.Generic;

/// <summary>
/// BaseRaiseEventCodes
/// </summary>
public class BaseRaiseEventCodes
{
	// --------
	#region メンバフィールド
	/// <summary>
	/// RaiseEvent送信テスト用コード
	/// </summary>
	public const byte SendTest = 0;
	#endregion //メンバフィールド

	// --------
	#region コンストラクタ
	/// <summary>
	/// 
	/// </summary>
	public BaseRaiseEventCodes()
	{

	}
	#endregion //コンストラクタ
}
