﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	public static GameManager Instance;
	/// <summary>
	/// 
	/// </summary>
	public GameObject m_PlayerPrefab;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		Instance = this;

		if(m_PlayerPrefab != null)
		{
			if(PlayerManager.LocalPlayerInstance == null)
			{
				PhotonNetwork.Instantiate(this.m_PlayerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
			}
		}
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド

	#region Photon Callbacks
	/// <summary>
	/// 
	/// </summary>
	public override void OnLeftRoom()
	{
		SceneManager.LoadScene(0);
	}
	/// <summary>
	/// プレイヤーが部屋に入ったときに呼ばれるコールバック
	/// </summary>
	/// <param name="other"></param>
	public override void OnPlayerEnteredRoom(Player other)
	{
		Debug.Log("mob- gamemanager- OnPlayerEnteredRoom() : "+other.NickName); // not seen if you're the player connecting

		if (PhotonNetwork.IsMasterClient)
		{
			Debug.Log("mob- gamemanager- OnPlayerEnteredRoom IsMasterClient : "+PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

			LoadArena();
		}
	}
	/// <summary>
	/// プレイヤーが部屋を去ったときに呼ばれるコールバック
	/// </summary>
	/// <param name="other"></param>
	public override void OnPlayerLeftRoom(Player other)
	{
		Debug.Log("mob- gamemanager- OnPlayerLeftRoom() : "+other.NickName); // seen when other disconnects

		if (PhotonNetwork.IsMasterClient)
		{
			Debug.Log("mob- gamemanager- OnPlayerLeftRoom IsMasterClient : "+PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

			LoadArena();
		}
	}
	#endregion //Photon Callbacks

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	public void LeaveRoom()
	{
		PhotonNetwork.LeaveRoom();
	}
	/// <summary>
	/// 
	/// </summary>
	protected void LoadArena()
	{
		if(!PhotonNetwork.IsMasterClient)
		{
			Debug.Log("mob- gamemanager- PhotonNetwork : Trying to Load a level but we are not the master Client");
		}
		Debug.Log("mob- gamemanager- PhotonNetwork : Loading Level : "+PhotonNetwork.CurrentRoom.PlayerCount);
		PhotonNetwork.LoadLevel("RoomFor" + PhotonNetwork.CurrentRoom.PlayerCount);
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
