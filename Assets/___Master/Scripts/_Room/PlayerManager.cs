﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	public static GameObject LocalPlayerInstance;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_Beams;
	/// <summary>
	/// 
	/// </summary>
	public float m_Health = 1.0f;
	/// <summary>
	/// 
	/// </summary>
	public float Health
	{
		get { return m_Health; }
		set { m_Health = value; }
	}
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PlayerUiPrefab;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	protected bool m_Firing;
	/// <summary>
	/// 
	/// </summary>
	public bool isFiring
	{
		get { return m_Firing; }
		protected set { m_Firing = value; }
	}
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
#if !UNITY_5_4_OR_NEWER
	void OnLevelWasLoaded(int level)
	{
		this.CalledOnLevelWasLoaded(level);
	}
#endif
	void CalledOnLevelWasLoaded(int level)
	{
		// check if we are outside the Arena and if it's the case, spawn around the center of the arena in a safe zone
		if (!Physics.Raycast(transform.position, -Vector3.up, 5f))
		{
			transform.position = new Vector3(0f, 5f, 0f);
		}

		GameObject _uiGo = Instantiate(m_PlayerUiPrefab);
		_uiGo.SendMessage("setTarget", this, SendMessageOptions.RequireReceiver);
	}
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		// used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
		if (photonView.IsMine)
		{
			PlayerManager.LocalPlayerInstance = this.gameObject;
		}
		// #Critical
		// we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
		DontDestroyOnLoad(this.gameObject);

		if(m_Beams != null)
		{
			m_Beams.SetActive(false);
		}
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		if(m_PlayerUiPrefab != null)
		{
			GameObject _uiGo = Instantiate(m_PlayerUiPrefab);
			_uiGo.SendMessage("setTarget", this, SendMessageOptions.RequireReceiver);
		}

		CameraWork _cameraWork = this.gameObject.GetComponent<CameraWork>();

		if (_cameraWork != null)
		{
			if (photonView.IsMine)
			{
				_cameraWork.OnStartFollowing();
			}
		}
#if UNITY_5_4_OR_NEWER
		UnityEngine.SceneManagement.SceneManager.sceneLoaded += (scene, loadingMode) =>
		{
			this.CalledOnLevelWasLoaded(scene.buildIndex);
		};
#endif
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{
		if (photonView.IsMine)
		{
			ProcessInputs();
		}
		if(m_Beams != null && isFiring != m_Beams.activeSelf)
		{
			m_Beams.SetActive(isFiring);
		}
		if(Health <= 0f)
		{
			GameManager.Instance.LeaveRoom();
		}
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	/// <summary>
	/// the on trigger enter.
	/// </summary>
	protected virtual void OnTriggerEnter(Collider other)
	{
		if(!photonView.IsMine)
		{
			return;
		}
		if(!other.name.Contains("Beam"))
		{
			return;
		}
		Health -= 0.1f;
	}
	/// <summary>
	/// the on trigger stay.
	/// </summary>
	protected virtual void OnTriggerStay(Collider other)
	{
		if (!photonView.IsMine)
		{
			return;
		}
		if (!other.name.Contains("Beam"))
		{
			return;
		}
		Health -= 0.1f * Time.deltaTime;
	}
	/// <summary>
	/// the on trigger exit.
	/// </summary>
	protected virtual void OnTriggerExit(Collider other)
	{

	}
	#endregion //MonoBehaviourメソッド

	#region IPunObservable implementation
	/// <summary>
	/// 
	/// </summary>
	/// <param name="stream"></param>
	/// <param name="info"></param>
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			// We own this player: send the others our data
			stream.SendNext(isFiring);
			stream.SendNext(Health);
		}
		else
		{
			// Network player, receive data
			isFiring = (bool)stream.ReceiveNext();
			Health = (float)stream.ReceiveNext();
		}
	}
	#endregion //IPunObservable implementation

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	protected virtual void ProcessInputs()
	{
		if(Input.GetKeyDown(KeyCode.L))
		{
			if(!isFiring)
			{
				isFiring = true;
			}
		}
		if(Input.GetKeyUp(KeyCode.L))
		{
			if(isFiring)
			{
				isFiring = false;
			}
		}
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
