﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerAnimatorManager : MonoBehaviourPun
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Animator m_Animator;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected float m_DirectionDampTime = 0.25f;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{
		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}
		if(m_Animator == null)
		{
			return;
		}
		AnimatorStateInfo stateInfo = m_Animator.GetCurrentAnimatorStateInfo(0);
		if(stateInfo.IsName("Base Layer.Run"))
		{
			if(Input.GetKeyDown(KeyCode.J))
			{
				m_Animator.SetTrigger("Jump");
			}
		}
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		if(v < 0)
		{
			v = 0;
		}
		m_Animator.SetFloat("Speed", h * h + v * v);
		m_Animator.SetFloat("Direction", h, m_DirectionDampTime, Time.deltaTime);
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
