﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Text m_PlayerNameText;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Slider m_PlayerHealthSlider;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Vector3 m_ScreenOffset = new Vector3(0,30.0f,0);
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	protected PlayerManager m_Target;
	/// <summary>
	/// 
	/// </summary>
	protected float m_CharacterControllerHeight = 0f;
	/// <summary>
	/// 
	/// </summary>
	protected Transform m_TargetTransform;
	/// <summary>
	/// 
	/// </summary>
	protected Vector3 m_TargetPosition;
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		this.transform.SetParent(GameObject.Find("CanvasLandscape").GetComponent<Transform>(), false);
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{
		if (m_Target == null)
		{
			Destroy(this.gameObject);
			return;
		}
		if(m_PlayerHealthSlider != null)
		{
			m_PlayerHealthSlider.value = m_Target.Health;
		}
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{
		if(m_TargetTransform != null)
		{
			m_TargetPosition = m_TargetTransform.position;
			m_TargetPosition.y += m_CharacterControllerHeight;
			this.transform.position = Camera.main.WorldToScreenPoint(m_TargetPosition) + m_ScreenOffset;
		}
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	public void setTarget(PlayerManager _target)
	{
		if(_target == null)
		{
			return;
		}
		m_Target = _target;
		m_TargetTransform = m_Target.GetComponent<Transform>();
		if(m_PlayerNameText != null)
		{
			m_PlayerNameText.text = m_Target.photonView.Owner.NickName;
		}
		CharacterController _characterController = _target.GetComponent<CharacterController>();
		if(_characterController != null)
		{
			m_CharacterControllerHeight = _characterController.height;
		}
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
