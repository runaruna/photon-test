﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

[RequireComponent(typeof(InputField))]
public class PlayerNameInputField : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	#region 定数
	/// <summary>
	/// 
	/// </summary>
	protected const string PLAYER_NAME_PREF_KEY = "PlayerName";
	#endregion //定数

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		string defaultName = string.Empty;
		InputField _inputFiled = this.GetComponent<InputField>();
		if(_inputFiled!=null)
		{
			if(PlayerPrefs.HasKey(PLAYER_NAME_PREF_KEY))
			{
				defaultName = PlayerPrefs.GetString(PLAYER_NAME_PREF_KEY);
				_inputFiled.text = defaultName;
			}
		}

		PhotonNetwork.NickName = defaultName;
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	/// <param name="_name"></param>
	public void setPlayerName(string _name)
	{
		if(string.IsNullOrEmpty(_name))
		{
			Debug.Log("mob- Player Name is null or empty...");
			return;
		}
		PhotonNetwork.NickName = _name;

		PlayerPrefs.SetString(PLAYER_NAME_PREF_KEY, _name);
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
