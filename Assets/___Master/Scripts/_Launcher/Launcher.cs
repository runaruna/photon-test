﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected string m_GameVersion = "1";
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected byte m_MaxPlayersPerRoom = 4;
	[Header("*UI : ControlPanel")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_LandscapeControlPanel;
	[Header("*UI : ProgressLabel")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_LandscapeProgressLabel;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// ユーザの手で接続を開始されたことを示すフラグ
	/// </summary>
	protected bool m_Connecting;
	/// <summary>
	/// [プロパティ]ユーザの手で接続を開始されたことを示すフラグ
	/// </summary>
	public bool isConnecting
	{
		get { return m_Connecting; }
		protected set { m_Connecting = value; }
	}
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		PhotonNetwork.AutomaticallySyncScene = true;
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		m_LandscapeProgressLabel.SetActive(false);
		m_LandscapeControlPanel.SetActive(true);
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド

	#region MonoBehaviourPunCallbacks
	/// <summary>
	/// 
	/// </summary>
	public override void OnConnectedToMaster()
	{
		Debug.Log("mob- launcher- OnConnectedToMaster() was called by PUN");

		if(isConnecting){
			PhotonNetwork.JoinRandomRoom();
		}
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="cause"></param>
	public override void OnDisconnected(DisconnectCause cause)
	{
		m_LandscapeProgressLabel.SetActive(false);
		m_LandscapeControlPanel.SetActive(true);

		Debug.LogWarningFormat("mob- launcher- OnDisconnected() was called by PUN with reason {0}", cause);
	}
	/// <summary>
	/// 
	/// </summary>
	/// <param name="returnCode"></param>
	/// <param name="message"></param>
	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		Debug.Log("mob- launcher- OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

		PhotonNetwork.CreateRoom(null, new RoomOptions{ MaxPlayers = m_MaxPlayersPerRoom });
	}
	/// <summary>
	/// 
	/// </summary>
	public override void OnJoinedRoom()
	{
		Debug.Log("mob- launcher- OnJoinedRoom() called by PUN. Now this client is in a room.");

		// #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
		if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
		{
			Debug.Log("We load the 'RoomFor1' ");

			// #Critical
			// Load the Room Level.
			PhotonNetwork.LoadLevel("RoomFor1");
		}
	}
	#endregion //MonoBehaviourPunCallbacks

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	public void Connect()
	{
		isConnecting = true;

		m_LandscapeProgressLabel.SetActive(true);
		m_LandscapeControlPanel.SetActive(false);

		if(PhotonNetwork.IsConnected)
		{
			PhotonNetwork.JoinRandomRoom();
		}
		else
		{
			PhotonNetwork.GameVersion = m_GameVersion;
			PhotonNetwork.ConnectUsingSettings();
		}
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
