﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyFPSController : MonoBehaviour {

	// --------
	#region インスペクタ設定用フィールド
	[Header("*Player Movement Setting")]
	/// <summary> 
	/// 
	/// </summary>
	[SerializeField] protected float m_PlayerMoveSpeed = 3.0f;
	[Header("*Player Rotation Setting")]
	/// <summary> 
	/// 
	/// </summary>
	[SerializeField] protected float m_PlayerRotateSpeed = 60.0f;
	#endregion

	// --------
	#region メンバフィールド
	/// <summary> 
	/// 
	/// </summary>
	protected Transform m_PlayerTransform;
	/// <summary>
	/// 
	/// </summary>
	protected Vector3 m_PlayerMoveVelocity;
	/// <summary> 
	/// 
	/// </summary>
	protected Vector3 m_PlayerMoveDirection;
	/// <summary> 
	/// 
	/// </summary>
	protected float m_PlayerUpAngleMax = 270.0f;
	/// <summary> 
	/// 
	/// </summary>
	protected float m_PlayerDownAngleMax = 90.0f;
	#endregion

	// --------
	#region MonoBehaviourメソッド
	/// <summary> 
	/// 初期化処理
	/// </summary>
	protected virtual void Awake() {
		m_PlayerTransform = this.gameObject.transform;
	}
	/// <summary> 
	/// 開始処理
	/// </summary>
	protected virtual void Start () {

	}
	/// <summary> 
	/// 更新処理
	/// </summary>
	protected virtual void Update () {
		playerMoveUpdate();
		playerRotateUpdate();
	}

	/// <summary> 
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate(){

	}

	/// <summary> 
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate(){

	}
	#endregion

	// --------
	#region メンバメソッド
	/// <summary>
	/// プレイヤー回転処理
	/// </summary>
	protected void playerRotateUpdate(){
		Vector3 m_PlayerNewAngle = new Vector3(
			m_PlayerTransform.localEulerAngles.x,
			m_PlayerTransform.localEulerAngles.y,
			m_PlayerTransform.localEulerAngles.z
		);

		//左回転
		if (Input.GetKey(KeyCode.LeftArrow)){
			m_PlayerNewAngle.y -= m_PlayerRotateSpeed * Time.deltaTime;
		}
		//右回転
		if (Input.GetKey(KeyCode.RightArrow)){
			m_PlayerNewAngle.y += m_PlayerRotateSpeed * Time.deltaTime;
		}
		//上回転
		if (Input.GetKey(KeyCode.UpArrow)){
			m_PlayerNewAngle.x -= m_PlayerRotateSpeed * Time.deltaTime;
			if(m_PlayerNewAngle.x > m_PlayerDownAngleMax && m_PlayerNewAngle.x < m_PlayerUpAngleMax){
				m_PlayerNewAngle.x = m_PlayerUpAngleMax;
			}
		}
		//下回転
		if (Input.GetKey(KeyCode.DownArrow)){
			m_PlayerNewAngle.x += m_PlayerRotateSpeed * Time.deltaTime;
			if(m_PlayerNewAngle.x < m_PlayerUpAngleMax && m_PlayerNewAngle.x > m_PlayerDownAngleMax){
				m_PlayerNewAngle.x = m_PlayerDownAngleMax;
			}
		}

		m_PlayerTransform.localEulerAngles = m_PlayerNewAngle;

	}

	/// <summary>
	/// プレイヤー移動処理
	/// </summary>
	protected virtual void playerMoveUpdate(){

		m_PlayerMoveVelocity = Vector3.zero;
		m_PlayerMoveDirection = Vector3.zero;

		if(Input.GetKey(KeyCode.W) &&
		 (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))){
			m_PlayerMoveDirection.y += 1.0f; //上平行移動
		}else if (Input.GetKey(KeyCode.W)){
			m_PlayerMoveDirection.z += 1.0f; //前進
		}

		if(Input.GetKey(KeyCode.S) &&
		 (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))){
			m_PlayerMoveDirection.y -= 1.0f; //上平行移動
		}else if (Input.GetKey(KeyCode.S)){
			m_PlayerMoveDirection.z -= 1.0f; //後退
		}

		if (Input.GetKey(KeyCode.A)){
			m_PlayerMoveDirection.x -= 1.0f; //左平行移動
		}

		if (Input.GetKey(KeyCode.D)){
			m_PlayerMoveDirection.x += 1.0f; //右平行移動
		}

		m_PlayerMoveVelocity = this.transform.TransformDirection(m_PlayerMoveDirection);
		m_PlayerMoveVelocity *= m_PlayerMoveSpeed; //移動スピード

		m_PlayerTransform.Translate(m_PlayerMoveVelocity, Space.World);
	}
	#endregion

	// --------
	#region インナークラス
	#endregion

}
