﻿using UnityEngine;
using System.Collections;

public class DestroyTimer : MonoBehaviour {

	public float destoryTime = 1.5f;

	// Use this for initialization
	void Start () {
		
		Destroy (this.gameObject, destoryTime);
	}
}
