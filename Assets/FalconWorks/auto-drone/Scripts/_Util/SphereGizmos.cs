using UnityEngine;
using System.Collections;

public class SphereGizmos : MonoBehaviour {

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// シーンギズモの色
	/// </summary>
	[SerializeField]
	private Color gizmosColor;
	/// <summary>
	/// シーンギズモのサイズ
	/// </summary>
	[SerializeField]
	private float gizmosSize = 1.0f;
	#endregion
	
	// --------
	#region MonoBehaviourメソッド
	void OnDrawGizmos(){

		//球を表示
		Gizmos.color = gizmosColor;
		Gizmos.DrawSphere (transform.position, gizmosSize);

	}
	#endregion
}
