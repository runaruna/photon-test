﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeGizmos : MonoBehaviour {

	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// ワイヤーフレーム表示
	/// </summary>
	[SerializeField] private bool m_WireframeMode;
	/// <summary>
	/// Gizmosの色
	/// </summary>
	[SerializeField] private Color m_GizmosColor;
	[SerializeField] private Vector3 m_GizmosCenter;
	[SerializeField] private Vector3 m_GizmosScale;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>

	#endregion //メンバフィールド

	// --------
	#region MonoBehaviourメソッド
	void OnDrawGizmos(){

		//球を表示
		Gizmos.color = m_GizmosColor;
		if(m_WireframeMode){
			Gizmos.DrawWireCube (transform.position + m_GizmosCenter , m_GizmosScale);
		}else{
			Gizmos.DrawCube(transform.position + m_GizmosCenter , m_GizmosScale);
		}

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
