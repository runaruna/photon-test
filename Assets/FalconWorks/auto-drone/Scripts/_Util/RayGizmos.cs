﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayGizmos : MonoBehaviour {

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// レイの色
	/// </summary>
	[SerializeField] private Color m_RayColor = new Color(0.0f,0.0f,255.0f,1.0f);

	/// <summary>
	/// レイの長さ
	/// </summary>
	[SerializeField, Range(0,100)] private float m_ArrowLenght = 1.0f;
	#endregion

	// --------
	#region メンバフィールド
	/// <summary> 
	/// 
	/// </summary>
	#endregion

	// --------
	#region MonoBehaviourメソッド

	void OnDrawGizmos(){

		//レイを表示
		Gizmos.color = m_RayColor;
		Gizmos.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * m_ArrowLenght);

	}
	#endregion

}
