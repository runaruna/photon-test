﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdDemoSceneDirector : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region 定数
	#endregion //定数

	// --------
	#region インスペクタ設定用フィールド
	[Header("*AdManager Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] AutoDroneManager m_AdManager;
	[Header("*UI StandardUIGroup Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] GameObject m_LandscapeStandardUIGroup;
	[Header("*UI AdPlaceModeUIGroup Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] GameObject m_LandscapeAdPlaceModeUIGroup;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// オートドローンのインスタンスIdを格納するフィールド
	/// </summary>
	protected int m_MyAdInstanceId;
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		m_AdManager.init((res)=>
		{
			Debug.Log("mob- sd- admanager-init- res:"+res);
		});
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	#endregion //MonoBehaviourメソッド


	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	public void onReceiveAdInstanceId(int _id)
	{
		Debug.Log("mob- sd- on-receive-ad-instance-id- _id:"+_id);
		m_MyAdInstanceId = _id;
	}

	public void onChgAdManagerModeState(int _mode)
	{
		switch (_mode)
		{
			case AdManagerModeState.PLACE:
				m_LandscapeStandardUIGroup.SetActive(false);
				m_LandscapeAdPlaceModeUIGroup.SetActive(true);
			break;
			case AdManagerModeState.WAIT:
			default:
				m_LandscapeStandardUIGroup.SetActive(true);
				m_LandscapeAdPlaceModeUIGroup.SetActive(false);
			break;
		}
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
