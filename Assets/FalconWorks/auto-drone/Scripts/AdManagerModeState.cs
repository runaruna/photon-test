﻿using System.Collections;
using System.Collections.Generic;

/// <summary>
/// オートドローン・マネージャのモード
/// </summary>
public class AdManagerModeState
{

	/// <summary>
	/// オートドローン設置モード
	/// </summary>
	public const int PLACE = 0;
	/// <summary>
	/// オートドローン削除モード
	/// </summary>
	public const int DELETE = 1;
	/// <summary>
	/// 待ち状態
	/// </summary>
	public const int WAIT = -9999;

	/// <summary>
	/// コンストラクタ
	/// </summary>
	AdManagerModeState()
	{

	}
}
