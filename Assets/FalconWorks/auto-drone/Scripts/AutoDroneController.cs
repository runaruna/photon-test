﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// オートドローンコントローラ
/// </summary>
public class AutoDroneController : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region 定数
	#endregion //定数

	// --------
	#region インスペクタ設定用フィールド
	[Header("*Model Settings")]
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_AutoDroneObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform AutoDroneTf 
	{
		get { return m_AutoDroneObj.transform; }
	}
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Collider m_Colider;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Rigidbody m_Rigidbody;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Vector3 m_DefaultScale;

	/// <summary>
	/// 待機制限時間
	/// </summary>
	[SerializeField] protected float m_WaitTimeLimit = 6.0f;
	[Header("*Move Settings")]
	/// <summary>
	/// 移動処理用レイキャストの起点の親
	/// </summary>
	[SerializeField] protected GameObject m_MoveRayOrgParentObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform MoveRayOrgParentTf 
	{
		get { return m_MoveRayOrgParentObj.transform; }
	}
	/// <summary>
	/// 移動処理用レイキャストの起点
	/// </summary>
	[SerializeField] protected GameObject m_MoveRayOrgObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform MoveRayOrgTf
	{
		get { return m_MoveRayOrgObj.transform; }
	}
	/// <summary>
	/// 移動処理用レイキャストの射程距離
	/// </summary>
	[SerializeField] protected float m_MoveRayDist = 1.0f;
	/// <summary>
	/// 障害物(ステージ壁等)のレイヤー名を格納する配列
	/// </summary>
	[SerializeField] protected string[] m_ObstacleLayerNames;
	/// <summary>
	/// 移動目的地オブジェクトのプレファブ
	/// </summary>
	[SerializeField] protected GameObject m_MoveDestPrefab;
	/// <summary>
	/// 最小移動距離
	/// </summary>
	[SerializeField] protected float m_MinMoveDist = 0.3f; 
	/// <summary>
	/// 最大移動スピード
	/// </summary>
	[SerializeField] protected float m_MaxSpd = 0.6f;
	/// <summary>
	/// 移動スピード
	/// </summary>
	[SerializeField] protected float m_AddMoveSpd = 0.02f;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected float m_AddMoveAngleLimit = 7.0f;
	/// <summary>
	/// 移動制限時間
	/// </summary>
	[SerializeField] protected float m_MoveTimeLimit = 10.0f;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_GroundRayOrgObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform GroundRayOrgTf
	{
		get { return m_GroundRayOrgObj.transform; }
	}
	/// <summary>
	///
	/// </summary>
	[SerializeField] protected float m_GroundRayDist = 0.03f;
	/// <summary>
	///
	/// </summary>
	[SerializeField] protected Vector3 m_GroundRayBoxHalfSize = new Vector3(0.0999556f, 0.00025f, 0.07532885f);
	/// <summary>
	///
	/// </summary>
	[SerializeField] protected string[] m_GroundLayerNames;
	[Header("*Debug Settings")]
	/// <summary>
	///
	/// </summary>
	[SerializeField] protected bool m_EnableDebugMoveDestFlag;
	/// <summary>
	///
	/// </summary>
	[SerializeField] protected GameObject m_DebugMoveDestPrefab;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 初期化完了フラグ
	/// </summary>
	protected bool m_InitCompFlag = false;
	/// <summary>
	/// [プロパティ] 初期化完了フラグ
	/// </summary>
	public bool InitCompFlag
	{
		get { return m_InitCompFlag; }
		protected set { m_InitCompFlag = value; }
	}
	/// <summary>
	/// 
	/// </summary>
	protected string m_CurrentADModeState;
	/// <summary>
	/// 待ち経過時間
	/// </summary>
	protected float m_WaitElapsedTime;
	#endregion //メンバフィールド

	// --------
	#region 移動関係のメンバフィールド
	/// <summary>
	/// 
	/// </summary>
	protected int m_ObstacleLayerMask;
	/// <summary>
	/// 
	/// </summary>
	protected GameObject m_MoveDestObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public GameObject MoveDestObj
	{
		get { return m_MoveDestObj; }
		protected set { m_MoveDestObj = value; }
	}
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform MoveDestTf
	{
		get { return m_MoveDestObj.transform; }
	}
	/// <summary>
	/// 移行更新処理有効フラグ
	/// </summary>
	protected bool m_EnableMoveUpdate;
	/// <summary>
	/// 移動中フラグ
	/// </summary>
	protected bool m_IsMoving;
	/// <summary>
	/// [プロパティ]移動中フラグ
	/// </summary>
	public bool IsMoving
	{
		get { return m_IsMoving; }
		protected set { m_IsMoving = value; }
	}
	/// <summary>
	/// 速度
	/// </summary>
	protected float m_Velocity;
	/// <summary>
	/// 移動経過時間
	/// </summary>
	protected float m_MoveElapsedTime;
	/// <summary>
	/// 
	/// </summary>
	protected int m_GroundLayerMask;
	/// <summary>
	/// 
	/// </summary>
	protected bool m_IsGround;
	/// <summary>
	/// 
	/// </summary>
	public bool IsGround
	{
		get { return m_IsGround; }
		protected set { m_IsGround = value; }
	}
	#endregion //移動関係のメンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		m_ObstacleLayerMask = LayerMask.GetMask(m_ObstacleLayerNames);
		m_GroundLayerMask = LayerMask.GetMask(m_GroundLayerNames);
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{
		groundRayUpdate();
		if(m_EnableMoveUpdate)
		{
			moveUpadte();
		}
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void FixedUpdate()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{
		lifeCycleUpdate();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region LifeCycle
	/// <summary>
	/// LifeCycleUpdate
	/// </summary>
	protected virtual void lifeCycleUpdate()
	{
		if(!InitCompFlag)
		{
			return;
		}

// Debug.Log("mob- lcupd- m_CurrentADModeState :"+m_CurrentADModeState);

		if(m_CurrentADModeState == ADModeState.Wait)
		{
			if(m_WaitElapsedTime > m_WaitTimeLimit)
			{
				m_CurrentADModeState = getRondomADModeState();
				m_WaitElapsedTime = 0;
				
				switch (m_CurrentADModeState)
				{
					case ADModeState.Move:
						searchMoveDestination((res)=>
						{
							if(res)
							{
								m_EnableMoveUpdate = true;
							}
							else
							{
								m_CurrentADModeState = ADModeState.Wait;
							}
						});
					break;
					case ADModeState.Blank:
						m_CurrentADModeState = ADModeState.Wait;
					break;
					default:
					break;
				}
			}
			else
			{
				m_WaitElapsedTime += Time.deltaTime;
			}
		}
	}
	/// <summary>
	/// ドローンのモードをランダムで返す関数
	/// </summary>
	/// <param name="_mode">1:タッチ時 2:マイク入力時</param>
	/// <returns></returns>
	protected virtual string getRondomADModeState()
	{
		string result = "";
		Dictionary<string, int> uniDict = new Dictionary<string, int>();

		uniDict.Add(ADModeState.Move, 80);//移動状態に移行する確率設定
		uniDict.Add(ADModeState.Blank, 20);//何もしない状態に移行する確率設定

		result = getRandomString(uniDict);

		//localのdictの中身を空にして自身をnullにする
		uniDict.Clear();
		uniDict = null;

		return result;
	}
	#endregion //LifeCycle

	// --------
	#region 移動処理関係
	/// <summary>
	/// 移動更新処理
	/// </summary>
	protected virtual void moveUpadte()
	{
		Debug.Log("mob- adc- mvupd-");

		Transform goal = null; //移動目的地OBJのTfを格納
		float dist = 0;
		float stopDist = 0.1f;
		float addAngleLimit = 7.0f;
		float addAngleY = 0;

		//移動目的地OBJをゴールに指定
		goal = MoveDestTf;
		dist = Vector3.Distance(goal.position, AutoDroneTf.position);//目的地との距離を取得

		if(dist >= stopDist)
		{
			IsMoving = true;
		}
		else
		{
			IsMoving = false;
		}

		//回転処理
		if(IsGround && IsMoving) //目的地に近すぎる場合は回転しないようにする
		{
			//現在の向きから向きたい方向へ何度回転すればいいか計算
			Vector3 aim = goal.position - AutoDroneTf.position;
			Vector3 look = Quaternion.LookRotation(aim).eulerAngles;

			//回転量を算出
			float y = Mathf.DeltaAngle(look.y, AutoDroneTf.eulerAngles.y); //横の回転量

			if (Mathf.Abs(y) < addAngleLimit)
			{
				// ±7度より小さければ回転しない
			}
			else if (y > 0)
			{
				addAngleY = -addAngleLimit; // プラスであれば左回り
			}
			else
			{
				addAngleY = +addAngleLimit; // マイナスであれば右回り
			}

			AutoDroneTf.localEulerAngles = new Vector3(
				AutoDroneTf.localEulerAngles.x,
				AutoDroneTf.localEulerAngles.y + addAngleY,
				AutoDroneTf.localEulerAngles.z
			);
		}

		//加速処理
		if (IsGround)
		{
			//走る
			if (m_Velocity > m_MaxSpd)
			{
				m_Velocity -= m_AddMoveSpd / 2.0f;
			}
			else
			{
				m_Velocity += m_AddMoveSpd;
			}
		}
		else
		{
			//止まる
			m_Velocity = 0;
		}
		AutoDroneTf.position += AutoDroneTf.forward * m_Velocity * Time.deltaTime;

		//ゴール判定
		if((m_MoveElapsedTime > m_MoveTimeLimit) || !IsMoving)
		{
			m_EnableMoveUpdate = false;
			m_MoveElapsedTime = 0;
			m_CurrentADModeState = ADModeState.Wait;
		}

		m_MoveElapsedTime += Time.deltaTime;
	}
	/// <summary>
	/// 移動目的地を探して移動目的地OBJをそこに移動させる
	/// </summary>
	/// <param name="callback"></param>
	protected virtual void searchMoveDestination(OnComp<bool> callback)
	{
		bool res = false;
		Vector3 moveDestPos = Vector3.zero;
		RaycastHit hit;
		Vector3 vec1 = Vector3.zero;
		Vector3 vec2 = Vector3.zero;
		Vector3 uavPos = new Vector3(0,25.0f,0);
		Dictionary<string, Vector3> rayDirectionDict = new Dictionary<string, Vector3>();
		rayDirectionDict.Add("f", Vector3.forward);
		rayDirectionDict.Add("b", Vector3.back);
		rayDirectionDict.Add("l", Vector3.left);
		rayDirectionDict.Add("r", Vector3.right);
		Dictionary<string, Vector3> uniDict = new Dictionary<string, Vector3>();
		float dist = 0;
		int searchRetryCnt = 0;
		int searchRetryLimit = 10;

		foreach (KeyValuePair<string, Vector3> kvp in rayDirectionDict)
		{
			if(Physics.Raycast(MoveRayOrgTf.position, kvp.Value, out hit, m_MoveRayDist, m_ObstacleLayerMask))
			{
				uniDict.Add(kvp.Key, hit.point);
			}
			else
			{
				Vector3 tmpVec = MoveRayOrgTf.position;
				Vector3 dc = kvp.Value * m_MoveRayDist;
				tmpVec = tmpVec + dc;
				uniDict.Add(kvp.Key, tmpVec);
			}
		}

		vec1.z = uniDict["f"].z;
		vec2.z = uniDict["b"].z;
		vec1.x = uniDict["l"].x;
		vec2.x = uniDict["r"].x;

		do
		{
			//移動目的地を範囲内からランダムで取得
			moveDestPos.x = Random.Range(vec1.x, vec2.x);
			moveDestPos.z = Random.Range(vec1.z, vec2.z);
			dist = Vector3.Distance(moveDestPos, AutoDroneTf.position);
			if (dist > m_MinMoveDist)
			{
				//上空25mよりレイを飛ばして目的地に地面があるかチェック
				uavPos.x = moveDestPos.x;
				uavPos.z = moveDestPos.z;
				if (Physics.SphereCast(uavPos, 0.083f, Vector3.down, out hit, 50.0f, m_GroundLayerMask))
				{
					res = true;
					moveDestPos = hit.point;
					MoveDestTf.position = moveDestPos; //目的地オブジェクトを移動
				}
			}
			++searchRetryCnt;
		}
		while (!res && (searchRetryCnt < searchRetryLimit));

		// Debug -----------------------
		if (m_EnableDebugMoveDestFlag)
		{
			Instantiate(
				m_DebugMoveDestPrefab,
				MoveDestTf.position,
				Quaternion.identity
			);
		}
		// -----------------------------

		rayDirectionDict.Clear();
		rayDirectionDict = null;
		uniDict.Clear();
		uniDict = null;

		callback(res);
	}
	/// <summary>
	/// 地面判定処理
	/// </summary>
	protected virtual void groundRayUpdate()
	{
		RaycastHit hit;

		if(Physics.BoxCast(
			GroundRayOrgTf.position,
			m_GroundRayBoxHalfSize,
			GroundRayOrgTf.TransformDirection(Vector3.down),
			out hit, AutoDroneTf.rotation,
			m_GroundRayDist, m_GroundLayerMask))
		{
			IsGround = true;
			// Debug.Log("mob- adc- gupd- IsGround : "+IsGround);
			if(hit.point.y > AutoDroneTf.position.y)
			{
				AutoDroneTf.position = new Vector3(
					AutoDroneTf.position.x,
					hit.point.y,
					AutoDroneTf.position.z
				);
			}
		}
		else
		{
			IsGround = false;
			// Debug.Log("mob- adc- gupd- IsGround : "+IsGround);
		}
	}
	#endregion //移動処理関係

	// --------
	#region 初期化・リセット関係
	/// <summary>
	/// 初期化
	/// </summary>
	public virtual void init()
	{
		m_MoveDestObj = (GameObject)Instantiate(
			m_MoveDestPrefab,
			Vector3.zero,
			Quaternion.identity
		);

		AutoDroneTf.localScale = m_DefaultScale;
		m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
		m_Rigidbody.useGravity = true;
		m_CurrentADModeState = ADModeState.Wait;
		InitCompFlag = true;
	}
	#endregion //初期化・リセット関係

	// --------
	#region メンバメソッド
	/// <summary>
	/// DictのstringタイプのKeyネームをValueの確率値を元に選定して返す関数
	/// </summary>
	/// <param name="_stringKeyDict"></param>
	/// <returns></returns>
	protected virtual string getRandomString(Dictionary<string, int> _stringKeyDict)
	{

		string result = string.Empty;
		int perTotal = 0;
		int randValue = 0;

		//累計確率
		foreach (KeyValuePair<string, int> item in _stringKeyDict)
		{
			perTotal += item.Value;
		}

		//1〜累計確率の間で乱数を作成
		randValue = Random.Range(1, perTotal + 1);

		//乱数から各確率を引いていき、0未満になったら終了
		foreach (KeyValuePair<string, int> kvp in _stringKeyDict)
		{
			randValue -= kvp.Value;
			if (randValue <= 0)
			{
				result = kvp.Key;
				break;
			}
		}

		return result;
	}
	/// <summary>
	/// DictのintタイプのKeyネームをValueの確率値を元に選定して返す関数
	/// </summary>
	/// <param name="_intKeyDict"></param>
	/// <returns></returns>
	protected virtual int getRandomInt(Dictionary<int, int> _intKeyDict)
	{

		int result = 0;
		int perTotal = 0;
		int randValue = 0;

		//累計確率
		foreach (KeyValuePair<int, int> item in _intKeyDict)
		{
			perTotal += item.Value;
		}

		//1〜累計確率の間で乱数を作成
		randValue = Random.Range(1, perTotal + 1);

		//乱数から各確率を引いていき、0未満になったら終了
		foreach (KeyValuePair<int, int> kvp in _intKeyDict)
		{
			randValue -= kvp.Value;
			if (randValue <= 0)
			{
				result = kvp.Key;
				break;
			}
		}

		return result;
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	/// <summary>
	/// ドローンのモード
	/// </summary>
	public class ADModeState
	{
		/// <summary>
		/// 待機状態
		/// </summary>
		public const string Wait = "Wait";
		/// <summary>
		/// 何もしない状態
		/// </summary>
		public const string Blank = "Blank";
		/// <summary>
		/// 移動状態
		/// </summary>
		public const string Move = "Move";
	}
	#endregion //インナークラス
}
