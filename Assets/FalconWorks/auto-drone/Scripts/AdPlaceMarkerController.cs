﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// オートドローン配置マーカーコントローラ
/// </summary>
public class AdPlaceMarkerController : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected Camera m_Camera;
	/// <summary>
	/// 
	/// </summary>
	[SerializeField] protected GameObject m_PivotObj;
	/// <summary>
	/// [プロパティ]
	/// </summary>
	public Transform PivotTf
	{
		get { return m_PivotObj.transform; }
	}
	/// <summary>
	/// マーカーのモデル
	/// </summary>
	[SerializeField] protected GameObject m_MarkerModelObj;
	[Header("*AdPlaceMarkerSensor Settings")]
	/// <summary>
	/// オートドローン配置マーカーセンサー
	/// </summary>
	[SerializeField] protected AdPlaceMarkerSensor m_AdPlaceMarkerSensor;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 配置可能フラグ
	/// </summary>
	public bool PlacePossible
	{
		get { return m_AdPlaceMarkerSensor.PlacePossible; }
	}
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		m_MarkerModelObj.SetActive(true);
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{
		//カメラが指定されていない場合はMainCameraを設定する
		if(this.m_Camera == null) m_Camera = Camera.main;
	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate ()
	{
		this.rotationUpdate();
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	/// <summary>
	/// 
	/// </summary>
	public virtual void showMarkerModel(bool _flg)
	{
		this.m_MarkerModelObj.SetActive(_flg);
	}
	/// <summary>
	/// 回転更新処理
	/// </summary>
	protected virtual void rotationUpdate()
	{
		if(m_Camera == null) return;

		//回転させる軸を制限
		Vector3 tPos = m_Camera.transform.position;
		tPos.y = PivotTf.position.y;

		PivotTf.LookAt(tPos);
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
