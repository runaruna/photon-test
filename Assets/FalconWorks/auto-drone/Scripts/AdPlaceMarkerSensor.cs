﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// オートドローン配置マーカーセンサー
/// </summary>
public class AdPlaceMarkerSensor : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region 定数
	#endregion //定数

	// --------
	#region インスペクタ設定用フィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 配置可能フラグ
	/// </summary>
	protected bool m_PlacePossible = true;
	/// <summary>
	/// [プロパティ]配置可能フラグ
	/// </summary>
	public bool PlacePossible
	{
		get { return m_PlacePossible; }
		protected set { m_PlacePossible = value; }
	}
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{

	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void LateUpdate()
	{

	}
	/// <summary>
	/// the on trigger stay.
	/// </summary>
	protected virtual void OnTriggerStay(Collider other)
	{
		this.PlacePossible = false;
	}
	/// <summary>
	/// the on trigger exit.
	/// </summary>
	protected virtual void OnTriggerExit(Collider other)
	{
		this.PlacePossible = true;
	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
