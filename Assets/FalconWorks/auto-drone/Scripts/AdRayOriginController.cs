﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// オートドローン・レイオリジンコントローラ
/// </summary>
public class AdRayOriginController : MonoBehaviour
{
	// --------
	#region iOS Native Plugin Methods
	#endregion //iOS Native Plugin Methods

	// --------
	#region 定数
	#endregion //定数

	// --------
	#region インスペクタ設定用フィールド
	[Header("*UI: ReticleWrapper Settings")]
	/// <summary>
	/// レティクルラッパー
	/// </summary>
	[SerializeField] protected GameObject m_ReticleWrapperObj;
	[Header("*UI: ReticleActive Settings")]
	/// <summary>
	/// レティクル・アクテイブ
	/// </summary>
	[SerializeField] protected GameObject m_ReticleActiveObj;
	[Header("*UI: ReticleInActive Settings")]
	/// <summary>
	/// レティクル・インアクテイブ
	/// </summary>
	[SerializeField] protected GameObject m_ReticleInActiveObj;
	#endregion //インスペクタ設定用フィールド

	// --------
	#region メンバフィールド
	/// <summary>
	/// 
	/// </summary>
	#endregion //メンバフィールド

	// --------
	#region delegate Type Member Fields
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComplete();  // delegate
	/// <summary>
	/// delegate型を宣言
	/// </summary>
	public delegate void OnComp<T>(T value); // delegate
	#endregion //delegate Type Member Fields

	// --------
	#region MonoBehaviourメソッド
	/// <summary>
	/// 初期化処理
	/// </summary>
	protected virtual void Awake()
	{
		this.m_ReticleWrapperObj.SetActive(false);
		this.m_ReticleActiveObj.SetActive(false);
		this.m_ReticleInActiveObj.SetActive(true);
	}
	/// <summary>
	/// 開始処理
	/// </summary>
	protected virtual void Start ()
	{

	}
	/// <summary>
	/// 更新処理
	/// </summary>
	protected virtual void Update ()
	{

	}
	#endregion //MonoBehaviourメソッド

	// --------
	#region メンバメソッド
	/// <summary>
	/// レティクルを表示
	/// </summary>
	public void showReticle(bool _flg)
	{
		m_ReticleWrapperObj.SetActive(_flg);
	}
	/// <summary>
	/// レティクルのアクテイブ・インアクティブ切り替え関数
	/// </summary>
	public void chgReticleActive(bool _flg)
	{
		m_ReticleActiveObj.SetActive(_flg);
		m_ReticleInActiveObj.SetActive(!_flg);
	}
	#endregion //メンバメソッド

	// --------
	#region インナークラス
	#endregion //インナークラス
}
